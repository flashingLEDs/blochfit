# BLOCHFIT 

A set of Igor Pro procedures for curve-fitting photoemission core-level and ARPES spectra. Models are provided for:
 - Fermi edge
 - Voigt singlets or doublets
 - Doniach-Sunjic singlets or doublets 
 - Voigt singlets plus Fermi edge

(Since the Voigt lineshape is a convolution of Gaussian and Lorentizan, Voigt models can be tuned to become purely Lorentizan or purely Gaussian models)

For all Voigt and Doniach-Sunjic models, an inelastic 'active' Shirley background is included (i.e. it is updated with the fit, not subtracted before fitting starts)

**Author:** Craig Polley (craig.polley@maxiv.lu.se)

**Version date:** 30/08/2018

**License:** Free software released into the public domain

## Setup

A copy of Igor Pro from Wavemetrics is required, and familiarity with using Igor is assumed. This version (15/08/2018) has been developed and tested in Igor 6.37. compatibility with newer versions has not yet been tested.

Gain access to the package by:
 1. loading the BLOCHFIT.ipf file into an Igor experiment (File > Open File > Procedure...)
 2. Pressing the *compile* button at the bottom of the BLOCHFIT.ipf window

You should now see a new menu entry in the top bar called BLOCHFIT

## Work flow overview

Working with BLOCHFIT consists of:
 1. Providing two waves (Intensity vs. Eb or vs. Ek) as input to the fitting routines. If your data is not already suitable for fitting, functions are available to extract sections from a 2D image or to crop a 1D spectrum.
 2. 'Preparing to fit' by choosing the model you want to try (e.g. 3 Voigt doublets). This should be done in a 'clean' data folder where you have not previously run BLOCHFIT.
 3. Finding optimal fit parameters by iteratively executing Igor's built-in Levenberg-Marquardt algorithm. 
 4. Generating a captioned graph including the raw data, fit, model components and residual.
 
## Code structure and where to find the peak models

The first ~700 lines deal with the overall framework. For each peak model, there are two functions. Taking the singlet voigt model as as example:

 1. Function BLOCHFIT_PrepareVoigt(numPeaks)
 2. Function BLOCHFIT_Voigt(pw,yw,xw) : FitFunc
 
The 'prepare' function generates the table of coefficients, the waves and variables needed for the fit, and generates a nicely formatted figure. In some cases makes a preliminary guess at starting coefficients.

The model itself is constructed in the second function. This is the function repeatedly called by Igor's fitting module as it tries to optimise the free coefficients. In many cases there are two separate models being built in this function: a 'normal' version for fitting to the data, and a higher resolution version using the same coefficients, purely for display purposes (This can be helpful if you want to plot a smooth looking model over a spectrum with a large stepsize) 

## Gaussian convolution

Convolution by a Gaussian lineshape is applied separately to each peak or Fermi edge, before adding it to the cumulative model. Igor's built-in algorithm for acausal linear convolution is used. 

The description used for the Gaussian is:

```math
Intensity = exp\Big(-4ln(2)(\frac{x}{FWHM})^2\Big)
```

Where $`x`$ is the energy scale centered on the feature being convolved, and FWHM is the full width at half maximum of the Gaussian. When comparing fit outputs with someone else's results, be mindful of the fact that sometimes different width metrics are used ($`\frac{1}{e^2}`$, 80%-20% width, standard deviation...)

## Shirley inelastic background

The inelastic background is added to the model after the peaks, but before adding the background offset and slope. It is created by a simple numerical integration over the model, linearly scaled by the Shirley background coefficient.


## Tutorial example 1: Fitting a Fermi edge

The data for this example is the wave named *Spectrum* in the *FermiEdge* folder of *Example.pxp*. It was acquired from a clean Au(111) single crystal at approximately 18K during the initial stages of comissioning the BLOCH beamline.

This data is provided as a 'raw' 2D image of angle vs energy. Plot it by right clicking and choosing NewImage. Add the info panel to make graph cursors available by either `ctrl+i` or by choosing Graph>Show Info from the top menu bar.

<img src="img/ef0.png" width="653px"/>


### Step 1: turn this image into a pair of waves ('Intensity' and 'Ek')

If you want to use the BLOCHFIT functions to do this, you have two options:

**Option 1 (simplest)**: Collapse the entire image along the angle axis.

Drag the A cursor onto the image plot (this helps BLOCHFIT identify the datawave to target)

<img src="img/ef1.gif" width="650px"/>


Then from the BLOCHFIT menu in the top bar select Extract 1D spectrum from 2D image>Collapse target image:

<img src="img/ef2.png" width="576px"/>


**Option 2: (best)** Manually select a subregion to extract

This time place both the A and B cursors on the image, using them as opposite corners of a square that defines the region to integrate over. For this it is helpful to have the cursor display mode on 'hair': 

<img src="img/ef4.gif" width="703px"/>


Then from the BLOCHFIT menu in the top bar select Extract 1D spectrum from 2D image>Extract EDC from subregion of image:

<img src="img/ef5.png" width="566px"/>


Both methods will generate a new 1D wave called 'EDC':

<img src="img/ef3.png" width="306px"/>


We still need to generate an 'Intensity' and 'Ek' wave. The simplest way to do this is to enter the following into the Igor command window (summon it to the front using `ctrl+j`):
```	
duplicate EDC Intensity,Ek
Ek=x
```

### Step 2: Prepare for fit

Let's try a simple Fermi edge fit:
<img src="img/ef6.png" width="528px"/>


This will generate a new figure and a new table. Once you've rearranged the cluttered windows, it should look like this:

<img src="img/ef7.png" width="549px"/>


Don't pay much attention to the graph yet; we haven't run a proper fit iteration yet so it won't look nice. Focus on the table. Here we have a list of the fit coefficient names, their values and finally a 'CoefLock' which will be explained shortly. 

### Step 3: Iteratively execute the fit

In this case the base model function is defined as:

```math
Intensity = \frac{Amplitude} {  exp(\frac{E_{K}-E_{F}}{k_{B}T}) + 1}
```

which is then convolved with a Gaussian to simulate experimental broadening, before finally adding a constant background offset.

Coefficients can be 'locked' by setting the corresponding element of BLOCHFIT_CoefLock to something other than zero. A locked coefficient will not be varied during the fitting process. Initially all coefficients are locked, so when you 'execute' the fit, the graph will simply draw the model as it looks with the coefficents in BLOCHFIT_Coefficients. 

To see this, change the sample temperature from 80 to 18 and the broadening from 0.05 to 0.02, but leave all coefficients locked. Then run 'Execute fit' from the BLOCHFIT top menu:

<img src="img/ef8.png" width="550px"/>

Notice how the green curve corresponding to the model has updated. In general this is an important strategy for successful fitting - manually tweak coefficients until the model is looking somewhat close to the data, and only then start unlocking coefficients. 

In this case we could now try unlocking the 'Broadening FWHM' parameter and running 'Execute fit' again:

<img src="img/ef9.png" width="550px"/>

Now we seem very close to the final model, so we could go ahead and unlock the remaining free parameters without much risk that the fitting algorithm will drive off a cliff. Unlock everything **except** 'Sample temperature' - we will generate a 'singular matrix' error if both 'Sample temperature' and 'Broadening' are unlocked at the same time, since they have identical broadening effects on the model. (You will anyway usually have a pretty good idea of the sample temperature from your logbook) 

<img src="img/ef10.png" width="550px"/>

That's it, you're done. If you would like to have those coefficients displayed on the graph, select 'Generate caption' from the BLOCHFIT top menu:

<img src="img/ef11.png" width="550px"/>

If you prefer different formatting for the caption, you can edit this in the BLOCHFIT_GenerateCaption() function

## Tutorial example 2: Fitting an Sn4d spectrum with a Voigt doublet

The data for this example is the pair of waves named Intensity and Eb in the VoigtDoublet folder of Example.pxp. It was acquired from a bulk (Pb,Sn)Se crystal buried under a few monolayers of PbSe.

This data is already suitable to start fitting. Notice that for peak models the x-wave is expected to be in binding energy, while the Fermi edge models required kinetic energy. Make VoigtDoublet the active directory, then prepare a Voigt doublet fit

<img src="img/vd0.png" width="700px"/>

This time more of the coefficients are left blank, forcing us to provide initial guesses. There is also a new column called BLOCHFIT_CoefTrack. This can be used to force a coefficient to stay the same as another coefficient. It is not useful for a single doublet like this, but for example you might be fitting three doublets which should all have the same spin-orbit splitting. The value in CoefTrack specifies the index of the coefficient it should track. By default, each coefficient is just set to track itself.

<img src="img/vd1.png" width="700px"/>

To start filling in the cofficients with initial guesses, drag the cursors onto the top of the 5/2 and 3/2 peaks. This will give you the primary peak position and the spin-orbit splitting:

<img src="img/vd2.png" width="700px"/>

You can also use the cursors to guess the size of the background offset and the peak amplitude:

<img src="img/vd3.png" width="700px"/>

If you now try to see how this guess looks by leaving all coefficients locked and selecting Execute Fit, you'll see that the background is the next thing we should deal with:

<img src="img/vd4.png" width="700px"/>

We could go two different ways here, adding either a background slope or making the inelastic background stronger. In this case we can be guided by the experimental details: these Sn photoelectrons are coming to our detector through a layer of PbSe, so it is reasonable to expect strong inelastic scattering. Unlock the Shirley Factor coefficient and Execute again:

<img src="img/vd5.png" width="700px"/>

Much better. It's already looking close, so unlock every coefficient except the slope and Execute again:

<img src="img/vd6.png" width="700px"/>

With that we're done, at least with this Voigt doublet model. Notice that the fit is not perfect, particularly around the tops of the peaks. This could either be a problem with the detector linearity, or that a Voigt doublet model is too simple. Only you can decide if a fit is good enough for your purposes.

## General comments on fitting strategy

This Sn4d doublet was quite simple, but things can quickly become very challenging when the experimental data looks strange and you're not sure how many peaks to try to fit it with. It is not unsual for a difficult spectrum to take many hours to fit satisfactorily, even days. By way of general advice:
1. **Always prefer the simplest possible model.** You can make a great looking fit to anything if you let the model consist of 20 peaks and a 100th order polynomial background. Avoid overfitting where possible. (This was the reason for leaving out the slope term in the Sn4d example)
2. **Always ask yourself whether your decision or result is physically reasonable**. Your spin-orbit splitting should be very close to literature values. Binding energies can be a little shifted, but should also be similar to literature. Your knowledge of the experimental setup will indicate a minimum level of experimental broadening to expect.
3. **Unlock coefficients gradually.** If you just dump 20 free coefficients into the fitting algorithm and the initial guesses are not very close to perfect, it will drive off a cliff
4. **Use checkpoints as a undo button.** BLOCHFIT gives you the option to save and reload a checkpoint, which means the values of the coefficients, locking and tracking. This lets you recover to an earlier state if a bad decision drove the fitting off a cliff.
5. **Detector non-linearity can skew the theoretically expected doublet intensity ratios**. Don't get too hung up on forcing a p doublet to have a branching ratio of exactly 0.5, for example.
6. **Uncertainties** BLOCHFIT will give you the uncertainties for the fit parameters. Treat these with extreme caution - they are calculated by Igor **assuming that your model is correct**. Since your model is almost never a perfect, complete description of the real physics going on, these uncertainty estimates are almost always too small.


