#pragma rtGlobals=3		// Use modern global access method and strict wave access.

// Author:          Craig Polley
// Version date:    17/09/2018
// License:         Free software released into the public domain

Menu "BLOCHFIT (17/09/2018)"

 	Submenu "Extract 1D spectrum from 2D image"
 		"Collapse target image", BLOCHFIT_CollapseImage()
 		"Extract EDC from subregion of image", BLOCHFIT_ExtractEDC()
 		"Extract MDC from subregion of image", BLOCHFIT_ExtractMDC()
 	End  
 	"Extract subregion of 1D spectrum", BLOCHFIT_CropSpectrum()

	"-"
 	Submenu "Prepare for fit"
 	
   		Submenu "Singlet voigt"
 			"1 peak", 	BLOCHFIT_PrepareVoigt(1)
 			"2 peaks", 	BLOCHFIT_PrepareVoigt(2)
 			"3 peaks", 	BLOCHFIT_PrepareVoigt(3)
 			"4 peaks", 	BLOCHFIT_PrepareVoigt(4)
 			"5 peaks", 	BLOCHFIT_PrepareVoigt(5)
 		End 
 		
     		Submenu "Singlet voigt plus Fermi edge"
 			"1 peak", 	BLOCHFIT_PrepareFermiVoigt(1)
 			"2 peaks", 	BLOCHFIT_PrepareFermiVoigt(2)
 			"3 peaks", 	BLOCHFIT_PrepareFermiVoigt(3)
 			"4 peaks", 	BLOCHFIT_PrepareFermiVoigt(4)
 			"5 peaks", 	BLOCHFIT_PrepareFermiVoigt(5)
 		End 
 				
   		Submenu "Singlet Doniach-Sunjic"
   			"1 peak", 	BLOCHFIT_PrepareDS(1)
   			"2 peaks", 	BLOCHFIT_PrepareDS(2)
   			"3 peaks", 	BLOCHFIT_PrepareDS(3)
   			"4 peaks", 	BLOCHFIT_PrepareDS(4)
 			"5 peaks", 	BLOCHFIT_PrepareDS(5)
 		End
 			
 		Submenu "Doublet voigt"
 			"1 doublet", 		BLOCHFIT_PrepareDoubletVoigt(1)
 			"2 doublets", 	BLOCHFIT_PrepareDoubletVoigt(2)
 			"3 doublets", 	BLOCHFIT_PrepareDoubletVoigt(3)
 			"4 doublets", 	BLOCHFIT_PrepareDoubletVoigt(4)
 			"5 doublets", 	BLOCHFIT_PrepareDoubletVoigt(5)
 		End

 		Submenu "Doublet Doniach-Sunjic"
 			"1 doublet", 	BLOCHFIT_PrepareDoubletDS(1)
 			"2 doublets", 	BLOCHFIT_PrepareDoubletDS(2)
 			"3 doublets", 	BLOCHFIT_PrepareDoubletDS(3)
 			"4 doublets", 	BLOCHFIT_PrepareDoubletDS(4)
 			"5 doublets", 	BLOCHFIT_PrepareDoubletDS(5)
 		End
 		
 		"Fermi edge [simple]", 	BLOCHFIT_PrepareSimpleFermiEdge()
 		"Fermi edge [resolution testing]", 	BLOCHFIT_PrepareFermiEdge()
  	 End 
  	 "-"
  	"Lock all coefficients", BLOCHFIT_CoefLock=1 	 
  	
  	"Save checkpoint", BLOCHFIT_SaveCheckpoint()
  	 "Reload checkpoint", BLOCHFIT_LoadCheckpoint()
  	 "Execute fit", BLOCHFIT_Execute()
  	 "-"
	 "Generate caption", BLOCHFIT_GenerateCaption()	
	"-"
 	"Rebuild a closed graph window",  BLOCHFIT_RebuildGraph()	
 	
   End 

//----------------------------------------------------------------
Function BLOCHFIT_CollapseImage()
//----------------------------------------------------------------

	wave SourceImage=CsrWaveRef(A)

	if (numtype(SourceImage[floor(DimSize(SourceImage,0)/2)][floor(DimSize(SourceImage,1)/2)])==2) //Go into the middle of the image and check if the element is NaN
		beep
		print ">>Error<< BLOCHFIT_CollapseImage couldn't identify the image to collapse, or the middle element is NaN"
		print "You must have the image plotted, with the A cursor on it, and it must be the most recently selected graph window" 
		return 0
	endif	
	
	variable EDC_length=DimSize(SourceImage,0)
	Make/O/N=(EDC_length)/D EDC
		
	SetScale/P x DimOffset(SourceImage,0),DimDelta(SourceImage,0),"", EDC

	variable Row,Column

	for(Column=0;Column<DimSize(SourceImage,1);Column+=1)
		for(Row=0;Row<DimSize(SourceImage,0);Row+=1)
			EDC[Row]+=SourceImage[Row][Column]
		endfor	
	endfor	
	
end

//----------------------------------------------------------------
Function BLOCHFIT_CropSpectrum()
//----------------------------------------------------------------
	wave SourceWave=CsrWaveRef(A)
	wave Eb=Eb
	
	if (numtype(Eb)==2) //NaN check
		beep
		print ">>Error<< BLOCHFIT_CropSpectrum couldn't find the binding energy wave, or the first element is NaN"
		print "You must have your data in the form of two waves, Intensity and Eb, and they must be located in the current data folder" 
		
	elseif (numtype(SourceWave[floor(DimSize(SourceWave,0)/2)])==2) //Go into the middle of the image and check if the element is NaN
		beep
		print ">>Error<< BLOCHFIT_CropSpectrum couldn't identify the spectrum to extract from, or the middle element is NaN"	
		print "You must have the spectrum plotted, with the A & B cursors on it, and it must be the most recently selected graph window" 
	
	elseif(pcsr(A) == NaN || pcsr(B) == NaN)
		beep
		print ">>Error<< Cursors are not both placed"
	elseif(pcsr(A) == pcsr(B))
		beep
		print ">>Error<< Length of the extracted region would be zero"
	else	
		variable newLength
		
		// p is the point number in the wave
		variable start_p, end_p
		
		// x,y are the real (eV) coordinates
		variable start_x, end_x
	
		// make sure the start and end pixel values are correctly ordered (i.e. small to high) since we will be iterating through them
		if(pcsr(A)<pcsr(B))
			start_p=pcsr(A)
			end_p=pcsr(B)
		else
			start_p=pcsr(B)
			end_p=pcsr(A)		
		endif
		
		// Do the same for the physical coordinates
		if(hcsr(A)<hcsr(B))	
			start_x=hcsr(A)
			end_x=hcsr(B)
		else
			start_x=hcsr(B)
			end_x=hcsr(A)		
		endif				
		
		print ">>Calling BLOCHFIT_CropSpectrum(), "+time()+" "+date()	
		print "\t\tExtracting subregion from spectrum: "+nameOfWave(SourceWave)
		print "\t\tExtraction range = ( "+num2str(start_x)+" , "+num2str(end_x)+" )"

		newLength=end_p - start_p

		
		Make/O/N=(newLength)/D Intensity_Cropped
		Make/O/N=(newLength)/D Eb_Cropped

		variable p
		
		for(p=start_p ; p<end_p ; p+=1)
			Intensity_Cropped[p-start_p]=SourceWave[p]
			Eb_Cropped[p-start_p]=Eb[p]
		endfor			
	endif
end	

//----------------------------------------------------------------
Function BLOCHFIT_ExtractEDC()
//----------------------------------------------------------------
	wave SourceImage=CsrWaveRef(A)

	if (numtype(SourceImage[floor(DimSize(SourceImage,0)/2)][floor(DimSize(SourceImage,1)/2)])==2) //Go into the middle of the image and check if the element is NaN
		beep
		print ">>Error<< BLOCHFIT_ExtractEDC couldn't identify the image to extract from, or the middle element is NaN"
		print "You must have the image plotted, with the A & B cursors on it, and it must be the most recently selected graph window" 
	
	elseif(pcsr(A) == NaN || pcsr(B) == NaN)
		beep
		print ">>Error<< Cursors are not both placed"
	elseif(pcsr(A) == pcsr(B))
		beep
		print ">>Error<< Length of the EDC would be zero"
	else	
		variable length
		// p,q are the array (pixel) coordinates
		variable start_p,end_p
		variable start_q,end_q
		// x,y are the real (eV,angle) coordinates
		variable start_x,end_x
		variable start_y, end_y
	
		// make sure the start and end pixel values are correctly ordered (i.e. small to high) since we will be iterating through them
		if(pcsr(A)<pcsr(B))
			start_p=pcsr(A)
			end_p=pcsr(B)
		else
			start_p=pcsr(B)
			end_p=pcsr(A)		
		endif
		
		if(qcsr(A)<qcsr(B))
			start_q=qcsr(A)
			end_q=qcsr(B)
		else
			start_q=qcsr(B)
			end_q=qcsr(A)			
		endif
		
		// Do the same for the physical coordinates
		if(vcsr(A)<vcsr(B))	
			start_x=vcsr(A)
			end_x=vcsr(B)
		else
			start_x=vcsr(B)
			end_x=vcsr(A)		
		endif
		
		if(hcsr(A)<hcsr(B))	
			start_y=hcsr(A)
			end_y=hcsr(B)
		else
			start_y=hcsr(B)
			end_y=hcsr(A)		
		endif				
		
		print ">>Calling BLOCHFIT_ExtractEDC(), "+time()+" "+date()	
		print "\t\tExtracting EDC from image: "+nameOfWave(SourceImage)
		print "\t\tExtraction y range = ( "+num2str(start_y)+" , "+num2str(end_y)+" )"
		print "\t\tExtraction x range = ( "+num2str(start_x)+" , "+num2str(end_x)+" )"

		length=(end_p - start_p)+1
		
		Make/O/N=(length)/D EDC
		
		SetScale/P x start_y,DimDelta(SourceImage,0),"", EDC

		EDC=0
		
		variable Row,Column
		
		for(Column=start_q;Column<end_q+1;Column+=1)
			for(Row=start_p;Row<end_p+1;Row+=1)
				EDC[Row-start_p]+=SourceImage[Row][Column]
			endfor	
		endfor		
		
	endif
end


//----------------------------------------------------------------
Function BLOCHFIT_ExtractMDC()
//----------------------------------------------------------------
	wave SourceImage=CsrWaveRef(A)
	
	if (numtype(SourceImage[floor(DimSize(SourceImage,0)/2)][floor(DimSize(SourceImage,1)/2)])==2) //Go into the middle of the image and check if the element is NaN
		beep
		print ">>Error<< BLOCHFIT_ExtractMDC couldn't identify the image to extract from, or the middle element is NaN"
		print "You must have the image plotted, with the A & B cursors on it, and it must be the most recently selected graph window" 
	
	elseif(pcsr(A) == NaN || pcsr(B) == NaN)
		beep
		print ">>Error<< Cursors are not both placed"

	elseif(qcsr(A) == qcsr(B))
		beep
		print ">>Error<< Length of the MDC would be zero"

	else	
		variable length
		// p,q are the array (pixel) coordinates
		variable start_p,end_p
		variable start_q,end_q
		// x,y are the real (eV,angle) coordinates
		variable start_x,end_x
		variable start_y, end_y
	
		// make sure the start and end pixel values are correctly ordered (i.e. small to high) since we will be iterating through them
		if(pcsr(A)<pcsr(B))
			start_p=pcsr(A)
			end_p=pcsr(B)
		else
			start_p=pcsr(B)
			end_p=pcsr(A)		
		endif
		
		if(qcsr(A)<qcsr(B))
			start_q=qcsr(A)
			end_q=qcsr(B)
		else
			start_q=qcsr(B)
			end_q=qcsr(A)			
		endif
		
		// Do the same for the physical coordinates
		if(vcsr(A)<vcsr(B))	
			start_x=vcsr(A)
			end_x=vcsr(B)
		else
			start_x=vcsr(B)
			end_x=vcsr(A)		
		endif
		
		if(hcsr(A)<hcsr(B))	
			start_y=hcsr(A)
			end_y=hcsr(B)
		else
			start_y=hcsr(B)
			end_y=hcsr(A)		
		endif
				
		print ">>Calling BLOCHFIT_ExtractMDC(), "+time()+" "+date()	
		print "\t\tExtracting MDC from image: "+nameOfWave(SourceImage)
		print "\t\tExtraction y range = ( "+num2str(start_y)+" , "+num2str(end_y)+" )"
		print "\t\tExtraction x range = ( "+num2str(start_x)+" , "+num2str(end_x)+" )"

		length=(end_q - start_q)+1
		
		Make/O/N=(length)/D MDC
		
		SetScale/P x start_x,DimDelta(SourceImage,1),"", MDC

		MDC=0
		
		variable Row,Column

		for(Row=start_p;Row<end_p+1;Row+=1)
			for(Column=start_q;Column<end_q+1;Column+=1)
				MDC[Column-start_q]+=SourceImage[Row][Column]
			endfor	
		endfor		
		
	endif
end

//----------------------------------------------------------
Function/S BLOCHFIT_BuildLockString(CoefLock)
//----------------------------------------------------------
	wave CoefLock
	string LockString=""
	variable ii
	wavestats/Q CoefLock
	for(ii=0;ii<v_npnts;ii=ii+1)
		if(CoefLock[ii]==1)
			LockString+="1"
		else
			LockString+="0"
		endif
	endfor
	
	return LockString
end




// Convolution causes some undesired edge effects (edges will want to bend down).
// We can avoid this by making the peak temporarily wider than normal, then cropping it back after convolution.
// Generate an x wave three times larger than the fitting range. 
//-------------------------------------------------------------------
Function BLOCHFIT_ExtendRange()
//-------------------------------------------------------------------
	WAVE BLOCHFIT_xw
	duplicate/O BLOCHFIT_xw temp_Convolution_yw
	wavestats /Q BLOCHFIT_xw
	
	Redimension/N=(V_npnts*3) temp_Convolution_yw
	
	variable NewOffset=BLOCHFIT_xw[0] - (V_npnts*(BLOCHFIT_xw[1]-BLOCHFIT_xw[0]))  // THIS ASSUMES THE ENERGY SPACING IS CONSTANT!
		
	SetScale/P x NewOffset,(BLOCHFIT_xw[1]-BLOCHFIT_xw[0]),"", temp_Convolution_yw
	//print "\tBLOCHFIT_ExtendRange: Input range is "+num2str(BLOCHFIT_xw[0])+" to "+num2str(BLOCHFIT_xw[V_npnts-1])
	//print "\tBLOCHFIT_ExtendRange: New range is "+num2str(NewOffset)+ " to "+num2str(NewOffset+(BLOCHFIT_xw[1]-BLOCHFIT_xw[0])*(V_npnts*3))
	//print "\tBLOCHFIT_ExtendRange: Delta is "+num2str(BLOCHFIT_xw[1]-BLOCHFIT_xw[0])
end

//-------------------------------------------------------------------
Function BLOCHFIT_Convolve(GaussFWHM,xw)
//-------------------------------------------------------------------
	variable GaussFWHM
	wave xw
	
	WAVE temp_Convolution_yw
	duplicate/O temp_Convolution_yw BLOCHFIT_Gaussian
	
 
	variable x0=xw[0]
	variable dx=(xw[1]-xw[0])
	Redimension/N=(numpnts(BLOCHFIT_Gaussian)+!mod(numpnts(BLOCHFIT_Gaussian), 2)) BLOCHFIT_Gaussian	// force GaussConvolver to be odd (required to center the gaussian peak)
	SetScale/P x, -dx*(numpnts(BLOCHFIT_Gaussian)-1)/2,dx,BLOCHFIT_Gaussian
	BLOCHFIT_Gaussian=exp(-4*ln(2)*((x)/GaussFWHM)^2) //Gaussian lineshape
		
	Convolve/A BLOCHFIT_Gaussian, temp_Convolution_yw
			
end

// Trim the wave back to the original size. Redimension cuts out the start of the wave, so shift the valid region to the end of the wave
//-------------------------------------------------------------------
Function BLOCHFIT_TrimRange(original_xw)
//-------------------------------------------------------------------
	wave original_xw
	WAVE temp_Convolution_yw
	wavestats/Q original_xw
	variable kk
	for(kk=V_npnts;kk<V_npnts*2;kk+=1)
		temp_Convolution_yw[kk-V_npnts]=temp_Convolution_yw[kk]
	endfor
	Redimension/N=(V_npnts) temp_Convolution_yw	
end
//----------------------------------------------------------
Function BLOCHFIT_SaveCheckpoint()
//----------------------------------------------------------
	execute "duplicate/O BLOCHFIT_Coefficients BLOCHFIT_Checkpoint"
	execute "duplicate/O BLOCHFIT_CoefLock BLOCHFIT_CoefLock_Checkpoint"
	execute "duplicate/O BLOCHFIT_CoefTrack BLOCHFIT_CoefTrack_Checkpoint"
end

//----------------------------------------------------------
Function BLOCHFIT_LoadCheckpoint()
//----------------------------------------------------------
	WAVE BLOCHFIT_Checkpoint,BLOCHFIT_CoefLock_Checkpoint,BLOCHFIT_CoefTrack_Checkpoint
	if(WaveExists(BLOCHFIT_Checkpoint) == 0 || WaveExists(BLOCHFIT_CoefLock_Checkpoint)  == 0 || WaveExists(BLOCHFIT_CoefTrack_Checkpoint)  == 0)
		beep
		print ">>Error<< Couldn't find the three checkpoint waves expected. Maybe you're trying to load without first saving?"
		print "Load operation aborted."
		return 0
	endif
	execute "BLOCHFIT_Coefficients=BLOCHFIT_Checkpoint"
	execute "BLOCHFIT_CoefLock=BLOCHFIT_CoefLock_Checkpoint"	
	execute "BLOCHFIT_CoefTrack=BLOCHFIT_CoefTrack_Checkpoint"	
end
//----------------------------------------------------------
Function BLOCHFIT_Execute()
//----------------------------------------------------------
	WAVE BLOCHFIT_Coefficients
	WAVE BLOCHFIT_CoefLock
	WAVE BLOCHFIT_CoefTrack
	
	variable ii
	wavestats/Q BLOCHFIT_Coefficients
	
	for(ii=0;ii<v_npnts;ii=ii+1)
		if(BLOCHFIT_CoefTrack[ii]!=ii)
			BLOCHFIT_CoefLock[ii]=1
		endif
	endfor
	
	string LockString=BLOCHFIT_BuildLockString(BLOCHFIT_CoefLock)
	NVAR BLOCHFIT_fitType
	NVAR BLOCHFIT_captionDrawn
	
	switch(BLOCHFIT_fitType)
		case 0: //Fermi edge for resolution testing
			execute "FuncFit/H=\""+LockString+"\"/NTHR=0 BLOCHFIT_FermiEdge BLOCHFIT_Coefficients  Intensity /X=Ek /D=BLOCHFIT_intensity /R=BLOCHFIT_residual"
		break
		
		case 1: //Singlet voigt
			execute "FuncFit/Q/H=\""+LockString+"\"/NTHR=0 BLOCHFIT_Voigt BLOCHFIT_Coefficients  Intensity /X=Eb /D=BLOCHFIT_intensity /R=BLOCHFIT_residual"
		break

		case 2: //Doublet voigt
			execute "FuncFit/Q/H=\""+LockString+"\"/NTHR=0 BLOCHFIT_DoubletVoigt BLOCHFIT_Coefficients  Intensity /X=Eb /D=BLOCHFIT_intensity /R=BLOCHFIT_residual"
		break

		case 3: //Doublet Doniach-Sunjic
			execute "FuncFit/Q/H=\""+LockString+"\"/NTHR=0 BLOCHFIT_DoubletDS BLOCHFIT_Coefficients  Intensity /X=Eb /D=BLOCHFIT_intensity /R=BLOCHFIT_residual"
			
		break
		
		case 4: //Singlet voigt with Fermi edge background
			execute "FuncFit/Q/H=\""+LockString+"\"/NTHR=0 BLOCHFIT_FermiVoigt BLOCHFIT_Coefficients  Intensity /X=Eb /D=BLOCHFIT_intensity /R=BLOCHFIT_residual"
		break
		
		case 5: //Singlet Doniach-Sunjic
			execute "FuncFit/Q/H=\""+LockString+"\"/NTHR=0 BLOCHFIT_DS BLOCHFIT_Coefficients  Intensity /X=Eb /D=BLOCHFIT_intensity /R=BLOCHFIT_residual"
		break	
		
		case 6: //Simple Fermi edge
			execute "FuncFit/H=\""+LockString+"\"/NTHR=0 BLOCHFIT_SimpleFermiEdge BLOCHFIT_Coefficients  Intensity /X=Ek /D=BLOCHFIT_intensity /R=BLOCHFIT_residual"
		break
							
		default:
			beep
			print ">>Error<< BLOCHFIT_Execute function doesn't know what kind of fit needs to be done"
			print "Try cleaning up and running a 'prepare for fit' function again"
			break
	endswitch

	for(ii=0;ii<v_npnts;ii=ii+1)
		if(BLOCHFIT_CoefTrack[ii]!=ii)
			BLOCHFIT_Coefficients[ii]=BLOCHFIT_Coefficients[BLOCHFIT_CoefTrack[ii]]
		endif
	endfor
		
	if(BLOCHFIT_captionDrawn==1)
		BLOCHFIT_GenerateCaption()
	endif	

	
end

//----------------------------------------------------------
Function BLOCHFIT_GenerateCaption()
//----------------------------------------------------------
	
	NVAR BLOCHFIT_fitType
	NVAR BLOCHFIT_captionDrawn
	WAVE BLOCHFIT_Coefficients
	WAVE W_sigma

	WAVE BLOCHFIT_Checkpoint,BLOCHFIT_CoefLock_Checkpoint,BLOCHFIT_CoefTrack_Checkpoint
	if(NVAR_Exists(BLOCHFIT_captionDrawn) == 0)
		beep
		print ">>Error<< You seem to be calling this from a subfolder that is missing some fit variables"
		print "Check that the 'current data folder' is correctly chosen. Otherwise your only option is to cleanly re-initialize a fit"
		print "Caption generation aborted."
		return 0
	endif
	
	string CaptionText
	string ExecuteText
	variable ii,jj
	
	switch(BLOCHFIT_fitType)
		case 0: //Fermi edge for resolution testing
			CaptionText = "\Z11\f01Fermi edge\f00"
			CaptionText += "\rFitting parameters ± one standard deviation:"		
			CaptionText += "\rAmplitude\t\t"+num2str(BLOCHFIT_Coefficients[0])+" ± "+num2str(W_sigma[0])
			CaptionText += "\rFermi Level (eV)\t"+num2str(BLOCHFIT_Coefficients[1])+" ± "+num2str(W_sigma[1])
			CaptionText += "\rTemperature (K)\t"+num2str(BLOCHFIT_Coefficients[2])+" ± "+num2str(W_sigma[2])
			CaptionText += "\rResolution (FWHM) \t"
			CaptionText += "\r\tSource (meV)\t\t"+num2str(BLOCHFIT_Coefficients[3]*1000)+" ± "+num2str(W_sigma[3]*1000)
			CaptionText += "\r\tAnalyzer (meV)\t"+num2str(BLOCHFIT_Coefficients[4]*1000)+" ± "+num2str(W_sigma[4]*1000)
			CaptionText += "\r\tTotal (meV)\t\t"+num2str(sqrt(BLOCHFIT_Coefficients[3]^2+BLOCHFIT_Coefficients[4]^2)*1000)
			CaptionText += "\rBackground offset\t"+num2str(BLOCHFIT_Coefficients[5])+" ± "+num2str(W_sigma[5])
			CaptionText += "\rBackground slope\t"+num2str(BLOCHFIT_Coefficients[6])+" ± "+num2str(W_sigma[6])
			CaptionText +="\r\r"+GetDataFolder(1)
			CaptionText +="\r"+time()+" "+date()	
			TextBox/C/N=text1 CaptionText
			Execute "SetDrawEnv xcoord= bottom,dash= 9;"
			ExecuteText="DrawLine "+num2str(BLOCHFIT_Coefficients[1])+",0,"+num2str(BLOCHFIT_Coefficients[1])+",1"
			Execute ExecuteText
		break
		
		case 1: //Singlet voigt
			NVAR BLOCHFIT_numPeaks
			CaptionText = "\Z10\f01"+num2str(BLOCHFIT_numPeaks)+" Voigt singlets\f00"
			CaptionText += "\rCaption generated at "+time()+" "+date()
			CaptionText += "\r\rData in "+GetDataFolder(1)
	
			CaptionText += "\r\r\f04Fitting parameters ± one standard deviation:\f00"	
			
			CaptionText += "\rPeak 0 "
			CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[0])+" ± "+num2str(W_sigma[0])
			CaptionText += "\r\tPosition (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[2])+" ± "+num2str(W_sigma[1])
			CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[3])+" ± "+num2str(W_sigma[2])
			CaptionText += "\r\tLorentzian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[4])+" ± "+num2str(W_sigma[3])		
			
			jj=4
			for(ii=1;ii<BLOCHFIT_numPeaks;ii+=1)
				CaptionText += "\rPeak"+num2str(ii)
				CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
				CaptionText += "\r\tPosition relative to peak 0 (eV)\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+1])
				CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+3])+" ± "+num2str(W_sigma[jj+2])
				CaptionText += "\r\tLorentzian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+4])+" ± "+num2str(W_sigma[jj+3])
				jj+=4	
			endfor
			CaptionText += "\rBackground"
			CaptionText += "\r\tShirley factor (counts per eV)\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
			CaptionText += "\r\tOffset (counts)\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
			CaptionText += "\r\tSlope (counts per eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])

			TextBox/C/N=text1 CaptionText
		break

		case 2: //Doublet voigt
			NVAR BLOCHFIT_numPeaks
			CaptionText = "\Z10\f01"+num2str(BLOCHFIT_numPeaks)+" Voigt doublets\f00"
			CaptionText += "\r"+time()+" "+date()+"\r"	
			CaptionText += "\rData in "+GetDataFolder(1)
	
			CaptionText += "\rFitting parameters ± one standard deviation:"	
			
			CaptionText += "\rDoublet 0 "
			CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[0])+" ± "+num2str(W_sigma[0])
			CaptionText += "\r\tBranching ratio\t\t\t"+num2str(BLOCHFIT_Coefficients[1])+" ± "+num2str(W_sigma[1])
			CaptionText += "\r\tPosition (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[2])+" ± "+num2str(W_sigma[2])
			CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[3])+" ± "+num2str(W_sigma[3])
			CaptionText += "\r\tLorentzian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[4])+" ± "+num2str(W_sigma[4])
			CaptionText += "\r\tSpin-orbit splitting (eV)\t\t"+num2str(BLOCHFIT_Coefficients[5])+" ± "+num2str(W_sigma[5])
			
			
			jj=6
			for(ii=1;ii<BLOCHFIT_numPeaks;ii+=1)
				CaptionText += "\rDoublet"+num2str(ii)
				CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
				CaptionText += "\r\tBranching ratio\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
				CaptionText += "\r\tPosition relative to d0 (eV)\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])
				CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+3])+" ± "+num2str(W_sigma[jj+3])
				CaptionText += "\r\tLorentzian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+4])+" ± "+num2str(W_sigma[jj+4])
				CaptionText += "\r\tSpin-orbit splitting (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+5])+" ± "+num2str(W_sigma[jj+5])		
				jj+=6	
			endfor
			CaptionText += "\rBackground"
			CaptionText += "\r\tShirley factor (counts per eV)\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
			CaptionText += "\r\tOffset (counts)\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
			CaptionText += "\r\tSlope (counts per eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])

			TextBox/C/N=text1 CaptionText
		break

		case 3: //Doublet DS
			NVAR BLOCHFIT_numPeaks
			CaptionText = "\Z10\f01"+num2str(BLOCHFIT_numPeaks)+" Doniach-Sunjic doublets\f00"
			CaptionText += "\r"+time()+" "+date()+"\r"	
			CaptionText += "\rData in "+GetDataFolder(1)
	
			CaptionText += "\rFitting parameters ± one standard deviation:"	
	
			CaptionText += "\rDoublet 0 "
			CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[0])+" ± "+num2str(W_sigma[0])
			CaptionText += "\r\tBranching ratio\t\t\t"+num2str(BLOCHFIT_Coefficients[1])+" ± "+num2str(W_sigma[1])
			CaptionText += "\r\tPosition (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[2])+" ± "+num2str(W_sigma[2])
			CaptionText += "\r\tAsymmetry (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[3])+" ± "+num2str(W_sigma[3])
			CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[4])+" ± "+num2str(W_sigma[4])
			CaptionText += "\r\tDoniach-Sunjic FWHM (eV)\t"+num2str(BLOCHFIT_Coefficients[5])+" ± "+num2str(W_sigma[5])
			CaptionText += "\r\tSpin-orbit splitting (eV)\t\t"+num2str(BLOCHFIT_Coefficients[6])+" ± "+num2str(W_sigma[6])
			
			jj=7
			for(ii=1;ii<BLOCHFIT_numPeaks;ii+=1)
				CaptionText += "\rDoublet"+num2str(ii)
				CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
				CaptionText += "\r\tBranching ratio\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
				CaptionText += "\r\tPosition relative to d0 (eV)\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])
				CaptionText += "\r\tAsymmetry (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+3])+" ± "+num2str(W_sigma[jj+3])
				CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+4])+" ± "+num2str(W_sigma[jj+4])
				CaptionText += "\r\tDoniach-Sunjic FWHM (eV)\t"+num2str(BLOCHFIT_Coefficients[jj+5])+" ± "+num2str(W_sigma[jj+5])
				CaptionText += "\r\tSpin-orbit splitting (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+6])+" ± "+num2str(W_sigma[jj+6])
				jj+=7	
			endfor
			CaptionText += "\rBackground"
			CaptionText += "\r\tShirley factor (counts per eV)\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
			CaptionText += "\r\tOffset (counts)\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
			CaptionText += "\r\tSlope (counts per eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])

			TextBox/C/N=text1 CaptionText
		break
		
		case 4: //Fermi Voigt
			NVAR BLOCHFIT_numPeaks
			CaptionText = "\Z08\f01"+num2str(BLOCHFIT_numPeaks)+" Voigt singlets plus Fermi edge\f00"
			CaptionText += "\r"+time()+" "+date()+"\r"	
			CaptionText += "\rData in "+GetDataFolder(1)
	
			CaptionText += "\rFitting parameters ± one standard deviation:"	

			for(ii=0;ii<BLOCHFIT_numPeaks;ii+=1)
				CaptionText += "\rPeak "+num2str(ii)
				CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[0])+" ± "+num2str(W_sigma[0])
				CaptionText += "\r\tPosition (eV)\t\t"+num2str(BLOCHFIT_Coefficients[1])+" ± "+num2str(W_sigma[1])
				CaptionText += "\r\tGaussian FWHM (eV)\t"+num2str(BLOCHFIT_Coefficients[2])+" ± "+num2str(W_sigma[2])
				CaptionText += "\r\tLorentzian FWHM (eV)\t"+num2str(BLOCHFIT_Coefficients[3])+" ± "+num2str(W_sigma[3])
				jj+=4	
			endfor
			
			CaptionText += "\rBackground"
			CaptionText += "\r\tOffset (counts)\t\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
			CaptionText += "\r\tSlope (counts per eV)\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
			CaptionText += "\r\tFermi level (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])
			CaptionText += "\r\tEffective temperature (K)\t"+num2str(BLOCHFIT_Coefficients[jj+3])+" ± "+num2str(W_sigma[jj+3])
			CaptionText += "\r\tDark counts\t\t"+num2str(BLOCHFIT_Coefficients[jj+4])+" ± "+num2str(W_sigma[jj+4])
			TextBox/C/N=text1 CaptionText
		break
		
		case 5: //Singlet DS
			NVAR BLOCHFIT_numPeaks
			CaptionText = "\Z10\f01"+num2str(BLOCHFIT_numPeaks)+" Doniach-Sunjic singlets\f00"
			CaptionText += "\r"+time()+" "+date()+"\r"	
			CaptionText += "\rData in "+GetDataFolder(1)
	
			CaptionText += "\rFitting parameters  ± one standard deviation:"	
	
			CaptionText += "\rPeak 0 "
			CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[0])+" ± "+num2str(W_sigma[0])
			CaptionText += "\r\tPosition (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[1])+" ± "+num2str(W_sigma[1])
			CaptionText += "\r\tAsymmetry (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[2])+" ± "+num2str(W_sigma[2])
			CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[3])+" ± "+num2str(W_sigma[3])
			CaptionText += "\r\tDoniach-Sunjic FWHM (eV)\t"+num2str(BLOCHFIT_Coefficients[4])+" ± "+num2str(W_sigma[4])
			jj=5
			for(ii=1;ii<BLOCHFIT_numPeaks;ii+=1)
				CaptionText += "\rPeak "+num2str(ii)
				CaptionText += "\r\tAmplitude (counts)\t\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
				CaptionText += "\r\tPosition relative to d0 (eV)\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
				CaptionText += "\r\tAsymmetry (eV)\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])
				CaptionText += "\r\tGaussian FWHM (eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+3])+" ± "+num2str(W_sigma[jj+3])
				CaptionText += "\r\tDoniach-Sunjic FWHM (eV)\t"+num2str(BLOCHFIT_Coefficients[jj+4])+" ± "+num2str(W_sigma[jj+4])
				jj+=5	
			endfor
			CaptionText += "\rBackground"
			CaptionText += "\r\tShirley factor (counts per eV)\t"+num2str(BLOCHFIT_Coefficients[jj])+" ± "+num2str(W_sigma[jj])
			CaptionText += "\r\tOffset (counts)\t\t\t"+num2str(BLOCHFIT_Coefficients[jj+1])+" ± "+num2str(W_sigma[jj+1])
			CaptionText += "\r\tSlope (counts per eV)\t\t"+num2str(BLOCHFIT_Coefficients[jj+2])+" ± "+num2str(W_sigma[jj+2])

			TextBox/C/N=text1 CaptionText

		break	
			
		case 6: //Simple Fermi edge
			CaptionText = "\Z11\f01Simple Fermi edge\f00"
			CaptionText += "\rFitting parameters  one standard deviation:"		
			CaptionText += "\rAmplitude\t\t\t"+num2str(BLOCHFIT_Coefficients[0])+" ± "+num2str(W_sigma[0])
			CaptionText += "\rFermi Level (eV)\t\t"+num2str(BLOCHFIT_Coefficients[1])+" ± "+num2str(W_sigma[1])
			CaptionText += "\rTemperature (K)\t\t"+num2str(BLOCHFIT_Coefficients[2])+" ± "+num2str(W_sigma[2])
			CaptionText += "\rEnergy broadening (meV)\t"+num2str(BLOCHFIT_Coefficients[3]*1000)+" ± "+num2str(W_sigma[3]*1000)
			CaptionText += "\rBackground offset\t\t"+num2str(BLOCHFIT_Coefficients[4])+" ± "+num2str(W_sigma[4])
			CaptionText +="\r\r"+GetDataFolder(1)
			CaptionText +="\r"+time()+" "+date()	
			TextBox/C/N=text1 CaptionText
			Execute "SetDrawEnv xcoord= bottom,dash= 9;"
			ExecuteText="DrawLine "+num2str(BLOCHFIT_Coefficients[1])+",0,"+num2str(BLOCHFIT_Coefficients[1])+",1"
			Execute ExecuteText
		break		
						
		default:
			beep
			print ">>Error<< BLOCHFIT_GenerateCaption function doesn't know what kind of caption needs to be made"
			print "Try cleaning up and running a 'prepare for fit' function again"
			break
	endswitch
		

	Execute "BLOCHFIT_captionDrawn=1"

End

//----------------------------------------------------------
Function BLOCHFIT_RebuildGraph()
//----------------------------------------------------------
	NVAR BLOCHFIT_numPeaks
	NVAR BLOCHFIT_fitType

	if(NVAR_Exists(BLOCHFIT_fitType) == 0)
		beep
		print ">>Error<< You seem to be calling this from a subfolder that is missing some fit variables"
		print "Check that the 'current data folder' is correctly chosen. Otherwise your only option is to cleanly re-initialize a fit"
		print "Caption generation aborted."
		return 0
	endif

	switch(BLOCHFIT_fitType)
		case 0: //Fermi edge for resolution testing
			BLOCHFIT_BuildFermiWindow()
		break
		
		case 1: // 	Singlet Voigt
			BLOCHFIT_BuildVoigtWindow(BLOCHFIT_numPeaks) 
		break
		
		case 2: // 	Doublet Voigt
			BLOCHFIT_Build2VoigtWindow(BLOCHFIT_numPeaks) 
		break
		
		case 3: //Doublet Doniach-Sunjic
			BLOCHFIT_Build2DSWindow(BLOCHFIT_numPeaks) 
		break
		
		case 4: //Singlet voigt with Fermi edge background
			BLOCHFIT_BuildFermiVoigtWindow(BLOCHFIT_numPeaks) 
		break
		
		case 5: //Singlet Doniach-Sunjic
			BLOCHFIT_BuildDSWindow(BLOCHFIT_numPeaks) 		
		break	
		
		case 6: //Simple Fermi edge
			BLOCHFIT_BuildSimpleEFWindow()
		break
				
		default:
			beep
			print ">>Error<< BLOCHFIT_RebuildGraph function doesn't know what kind of graph needs to be made"
			print "Try cleaning up and running a 'prepare for fit' function again"
		break		
				
	endswitch


end

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 	Singlet Voigt
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------
Function BLOCHFIT_PrepareVoigt(numPeaks)
//----------------------------------------------------------

	variable numPeaks
	execute "variable/G BLOCHFIT_numPeaks="+num2str(numPeaks)
	
	WAVE Intensity
	WAVE Eb

	if(WaveExists(Intensity) == 0 || WaveExists(Eb) == 0)
		beep
		print ">>Error<< for this fit function BLOCHFIT expects to find two specifically named input waves: [Intensity] and [Eb]"
		print "Waves with this name couldn't be found, so preparation was aborted"
		return 0
	endif
		
	variable ParametersPerPeak=4		// Amplitude, position, Gauss FWHM, Lorentz FWHM
	variable NumExtraParameters=3	// Shirley, BG offset, BG slope
	 
	variable NumParameters=(numPeaks*ParametersPerPeak) + NumExtraParameters	
	
	variable ii,jj
	string tempString
	Make/N=(NumParameters)/T/O BLOCHFIT_CoefficientNames	
	jj=0
	
	tempString="(Peak "+num2str(ii)+")"
	BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
	BLOCHFIT_CoefficientNames[jj+1]=tempString+" position (eV)"
	BLOCHFIT_CoefficientNames[jj+2]=tempString+" Gaussian FWHM (eV)"
	BLOCHFIT_CoefficientNames[jj+3]=tempString+" Lorentzian FWHM (eV)"
	jj+=ParametersPerPeak;
	
	for(ii=1;ii<numPeaks;ii+=1)
		tempString="(Peak "+num2str(ii)+")"
		BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
		BLOCHFIT_CoefficientNames[jj+1]=tempString+" position relative to Peak 0 (eV)"
		BLOCHFIT_CoefficientNames[jj+2]=tempString+" Gaussian FWHM (eV)"
		BLOCHFIT_CoefficientNames[jj+3]=tempString+" Lorentzian FWHM (eV)"
		jj+=ParametersPerPeak;
	endfor	
	BLOCHFIT_CoefficientNames[jj]="Shirley factor (counts per eV)"
	BLOCHFIT_CoefficientNames[jj+1]="Background offset (counts)"
	BLOCHFIT_CoefficientNames[jj+2]="Background slope (counts per eV)"
	
	
	// We can make some reasonable starting guesses for things like width and background. Otherwise leave as NaN so user has to fill in
	Make/N=(NumParameters)/D/O BLOCHFIT_Coefficients
	jj=0
	for(ii=0;ii<numPeaks;ii+=1)
		BLOCHFIT_Coefficients[jj]=NaN
		BLOCHFIT_Coefficients[jj+1]=NaN
		BLOCHFIT_Coefficients[jj+2]=0.3
		BLOCHFIT_Coefficients[jj+3]=0.3
		jj+=ParametersPerPeak;
	endfor
	BLOCHFIT_Coefficients[jj]=0.001
	BLOCHFIT_Coefficients[jj+1]=0
	BLOCHFIT_Coefficients[jj+2]=0
	
	
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefLock
	BLOCHFIT_CoefLock=1
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefTrack
	BLOCHFIT_CoefTrack=x	
	
	// Make the waves we will use in the fitting graph.Some we can make higher resolution
	wavestats /Q Intensity
	Duplicate/O Intensity BLOCHFIT_Eb_display
	Redimension/N=(V_npnts*10) BLOCHFIT_Eb_display
	SetScale /I x, Eb[0], Eb[V_npnts-1], BLOCHFIT_Eb_display	//Make it have the same range
	BLOCHFIT_Eb_display=x
	
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Fit_display
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Background_display

	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Peak"+num2str(ii)+"_display"
		Execute "Duplicate/O BLOCHFIT_Eb_display "+tempString
		tempString="BLOCHFIT_Peak"+num2str(ii)
		Execute "Duplicate/O Intensity "+tempString		
	endfor
	
	// Others we need in the same resolution
	Duplicate/O Intensity BLOCHFIT_residual, BLOCHFIT_intensity

	Execute "variable/G BLOCHFIT_captionDrawn;BLOCHFIT_captionDrawn=0"
	Execute "variable/G BLOCHFIT_fitType;BLOCHFIT_fitType=1"

	BLOCHFIT_BuildVoigtWindow(numPeaks)

end


//----------------------------------------------------------
Function BLOCHFIT_BuildVoigtWindow(numPeaks)
//----------------------------------------------------------
	variable numPeaks
	variable ii
	string tempString
	// Build the fitting graph
	Execute "Display BLOCHFIT_Fit_display,BLOCHFIT_Background_display vs BLOCHFIT_Eb_display"
	
	Execute "AppendToGraph Intensity vs Eb"
	Execute "AppendToGraph/L=L2 BLOCHFIT_residual vs Eb"
	Execute "ModifyGraph zero(L2)=3,axisEnab(left)={0.15,1},axisEnab(L2)={0,0.1},freePos(L2)=0"
	Execute "ModifyGraph mode(Intensity)=3, marker(Intensity)=19, msize(Intensity)=1.5, rgb(Intensity)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_residual)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_Fit_display)=(2,39321,1)"
	Execute "ModifyGraph mode(BLOCHFIT_Background_display)=7,hbFill(BLOCHFIT_Background_display)=5, rgb(BLOCHFIT_Background_display)=(26112,26112,26112)"
	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Peak"+num2str(ii)+"_display"
		Execute "AppendToGraph "+tempstring+" vs BLOCHFIT_Eb_display "
	endfor	
	Execute "Label bottom \"Binding energy (eV)\";Label left \"Intensity\""
	Execute "Legend/C/N=text0/J \"\""
	Execute "AppendText/N=text0 \"\\s(Intensity) Data\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Background_display) Background\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Fit_display) Fit\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Residual) Residual\""
	for(ii=0;ii<numPeaks;ii+=1)
		tempstring="AppendText/N=text0 \"\\s(BLOCHFIT_Peak"+num2str(ii)+"_display) Peak "+num2str(ii)+"\""
		Execute tempstring
	endfor
	Execute "Edit/K=0  BLOCHFIT_CoefficientNames,BLOCHFIT_Coefficients,BLOCHFIT_CoefLock,BLOCHFIT_CoefTrack"
	Execute "SetAxis/A/R bottom"
end


//-------------------------------------------------------------------
Function BLOCHFIT_Voigt(pw,yw,xw) : FitFunc
//-------------------------------------------------------------------
	Wave pw,yw,xw
	
	variable peakIndex,coefficientOffset
	
	variable Amplitude,Position,GaussFWHM,LorentzianFWHM
	
	// Waves and variables that should already exist
	WAVE Intensity,Eb
	WAVE BLOCHFIT_Background_display
	WAVE BLOCHFIT_Eb_display
	WAVE BLOCHFIT_Fit_display
	NVAR BLOCHFIT_numPeaks
	WAVE BLOCHFIT_CoefTrack
	
	BLOCHFIT_Fit_display=0
	yw=0
	
	// For each peak, make a Lorentzian convolved with a Gaussian and add it to the fit output
	// Do it twice: once for the actual fit and once for the higher resolution display traces

	//print "Performing Voigt fit with "+num2str(BLOCHFIT_numPeaks)+ " singlet peaks"
	coefficientOffset=0
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex))
		
		Amplitude=pw[BLOCHFIT_CoefTrack[coefficientOffset]]
		Position=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]]
		if(coefficientOffset>0)
			Position=Position+pw[BLOCHFIT_CoefTrack[1]]	// Position is given relative to peak 0
		endif	
		GaussFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]
		LorentzianFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+3]]
		
		//print "Peak #"+num2str(peakIndex)+", Amplitude="+num2str(Amplitude)
		// --- Fitting peak
		duplicate/O xw BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,xw)
		BLOCHFIT_TrimRange(xw)
		//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
		wavestats/Q temp_Convolution_yw
		Peak=(temp_Convolution_yw/V_max)*Amplitude
		yw+=Peak
		killwaves temp_Convolution_yw
	
		// --- Display peak
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex)+"_display")
		duplicate/O BLOCHFIT_Eb_display BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,BLOCHFIT_Eb_display)
		BLOCHFIT_TrimRange(BLOCHFIT_Eb_display)
		wavestats/Q temp_Convolution_yw
		Peak=(temp_Convolution_yw/V_max)*Amplitude	
		BLOCHFIT_Fit_display+=Peak
		killwaves temp_Convolution_yw
	
		coefficientOffset+=4
	endfor

	// ------ Generate Shirley background ------ 
	variable ShirleyCoefficient=pw[BLOCHFIT_CoefTrack[coefficientOffset]] // Counts per eV
	variable IntegratedIntensity=0
	
	// 
	
	// --- For the fit:
	duplicate/O Intensity ShirleyBackground
	ShirleyBackground=0
	wavestats/Q yw
	variable ii
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+ (xw[ii-1]-xw[ii])*((yw[ii]+yw[ii-1])/2)
		ShirleyBackground[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	ShirleyBackground[0]=ShirleyBackground[1]
	yw+=ShirleyBackground
	
	// --- For display:
	wavestats/Q BLOCHFIT_Fit_display
	IntegratedIntensity=0
	
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+  (BLOCHFIT_Eb_display[ii-1]-BLOCHFIT_Eb_display[ii])*((BLOCHFIT_Fit_display[ii]+BLOCHFIT_Fit_display[ii-1])/2)
		BLOCHFIT_Background_display[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	BLOCHFIT_Background_display[0]=BLOCHFIT_Background_display[1]
	
		
	// -----------------------------------------------------------------
	
		
	// Add the background

	variable BG_offset=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]] // Counts
	variable BG_slope=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]	
	yw = yw + BG_offset + BG_slope*(xw-xw[numpnts(xw) - 1])
	BLOCHFIT_Background_display=BLOCHFIT_Background_display+ BG_offset + BG_slope*(BLOCHFIT_Eb_display-BLOCHFIT_Eb_display[numpnts(BLOCHFIT_Eb_display) - 1])
	BLOCHFIT_Fit_display=BLOCHFIT_Fit_display+BLOCHFIT_Background_display
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex)+"_display")
		Peak=Peak+BLOCHFIT_Background_display
	endfor
end


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 	Singlet Voigt plus Fermi edge
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------
Function BLOCHFIT_PrepareFermiVoigt(numPeaks)
//----------------------------------------------------------

	variable numPeaks
	execute "variable/G BLOCHFIT_numPeaks="+num2str(numPeaks)
	
	
	WAVE Intensity
	WAVE Eb
	
	if(WaveExists(Intensity) == 0 || WaveExists(Eb) == 0)
		beep
		print ">>Error<< for this fit function BLOCHFIT expects to find two specifically named input waves: [Intensity] and [Eb]"
		print "Waves with this name couldn't be found, so preparation was aborted"
		return 0
	endif
	
	variable ParametersPerPeak=4		// Amplitude, position, Gauss FWHM, Lorentz FWHM, 
	variable NumExtraParameters=5	// Background offset, Background slope,  Fermi level, effective sample temperature, dark counts
	 
	variable NumParameters=(numPeaks*ParametersPerPeak) + NumExtraParameters	
	
	variable ii,jj
	string tempString
	Make/N=(NumParameters)/T/O BLOCHFIT_CoefficientNames	
	
	jj=0
	
	for(ii=0;ii<numPeaks;ii+=1)
		tempString="(Peak "+num2str(ii)+")"
		BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
		BLOCHFIT_CoefficientNames[jj+1]=tempString+" position (eV)"
		BLOCHFIT_CoefficientNames[jj+2]=tempString+" Gaussian FWHM (eV)"
		BLOCHFIT_CoefficientNames[jj+3]=tempString+" Lorentzian FWHM (eV)"
		jj+=ParametersPerPeak;
	endfor	
	
	BLOCHFIT_CoefficientNames[jj]="Background offset (counts)"
	BLOCHFIT_CoefficientNames[jj+1]="Background slope (counts per eV)"
	BLOCHFIT_CoefficientNames[jj+2]="Fermi level position (eV)"
	BLOCHFIT_CoefficientNames[jj+3]="Effective sample temperature (K)"
	BLOCHFIT_CoefficientNames[jj+4]="Dark counts (counts)"
	
	// We can make some reasonable starting guesses for things like width and background. Otherwise leave as NaN so user has to fill in
	Make/N=(NumParameters)/D/O BLOCHFIT_Coefficients
	jj=0
	for(ii=0;ii<numPeaks;ii+=1)
		BLOCHFIT_Coefficients[jj]=NaN
		BLOCHFIT_Coefficients[jj+1]=NaN
		BLOCHFIT_Coefficients[jj+2]=0.3
		BLOCHFIT_Coefficients[jj+3]=0.3
		jj+=ParametersPerPeak;
	endfor
	BLOCHFIT_Coefficients[jj]=NaN
	BLOCHFIT_Coefficients[jj+1]=0
	BLOCHFIT_Coefficients[jj+2]=0
	BLOCHFIT_Coefficients[jj+3]=300
	BLOCHFIT_Coefficients[jj+4]=0
	
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefLock
	BLOCHFIT_CoefLock=1
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefTrack
	BLOCHFIT_CoefTrack=x	
	
	// Make the waves we will use in the fitting graph.Some we can make higher resolution
	wavestats /Q Intensity
	Duplicate/O Intensity BLOCHFIT_Eb_display
	Redimension/N=(V_npnts*10) BLOCHFIT_Eb_display
	SetScale /I x, Eb[0], Eb[V_npnts-1], BLOCHFIT_Eb_display	//Make it have the same range
	BLOCHFIT_Eb_display=x
	
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Fit_display
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Background_display

	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Peak"+num2str(ii)+"_display"
		Execute "Duplicate/O BLOCHFIT_Eb_display "+tempString
		tempString="BLOCHFIT_Peak"+num2str(ii)
		Execute "Duplicate/O Intensity "+tempString		
	endfor
	
	// Others we need in the same resolution
	Duplicate/O Intensity BLOCHFIT_residual, BLOCHFIT_intensity


	Execute "variable/G BLOCHFIT_captionDrawn;BLOCHFIT_captionDrawn=0"
	Execute "variable/G BLOCHFIT_fitType;BLOCHFIT_fitType=4"
	
	 BLOCHFIT_BuildFermiVoigtWindow(numPeaks)

End

//----------------------------------------------------------
Function BLOCHFIT_BuildFermiVoigtWindow(numPeaks)
//----------------------------------------------------------
	variable numPeaks
	variable ii
	string tempString

	// Build the fitting graph
	Execute "Display BLOCHFIT_Fit_display,BLOCHFIT_Background_display vs BLOCHFIT_Eb_display"
	
	Execute "AppendToGraph Intensity vs Eb"
	Execute "AppendToGraph/L=L2 BLOCHFIT_residual vs Eb"
	Execute "ModifyGraph zero(L2)=3,axisEnab(left)={0.15,1},axisEnab(L2)={0,0.1},freePos(L2)=0"
	Execute "ModifyGraph mode(Intensity)=3, marker(Intensity)=19, msize(Intensity)=1.5, rgb(Intensity)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_residual)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_Fit_display)=(2,39321,1)"
	Execute "ModifyGraph mode(BLOCHFIT_Background_display)=7,hbFill(BLOCHFIT_Background_display)=5, rgb(BLOCHFIT_Background_display)=(26112,26112,26112)"
	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Peak"+num2str(ii)+"_display"
		Execute "AppendToGraph "+tempstring+" vs BLOCHFIT_Eb_display "
	endfor		
	Execute "Label bottom \"Binding energy (eV)\";Label left \"Intensity\""
	Execute "Legend/C/N=text0/J \"\""
	Execute "AppendText/N=text0 \"\\s(Intensity) Data\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Background_display) Background\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Fit_display) Fit\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Residual) Residual\""
	for(ii=0;ii<numPeaks;ii+=1)
		tempstring="AppendText/N=text0 \"\\s(BLOCHFIT_Peak"+num2str(ii)+"_display) Peak "+num2str(ii)+"\""
		Execute tempstring
	endfor
	Execute "Edit/K=0  BLOCHFIT_CoefficientNames,BLOCHFIT_Coefficients,BLOCHFIT_CoefLock,BLOCHFIT_CoefTrack"
	Execute "SetAxis/A/R bottom"
		
End

//-------------------------------------------------------------------
Function BLOCHFIT_FermiVoigt(pw,yw,xw) : FitFunc
//-------------------------------------------------------------------
	Wave pw,yw,xw
	
	variable peakIndex,coefficientOffset
	
	variable Amplitude,Position,GaussFWHM,LorentzianFWHM
	
	// Waves and variables that should already exist
	WAVE Intensity,Eb
	WAVE BLOCHFIT_Background_display
	WAVE BLOCHFIT_Eb_display
	WAVE BLOCHFIT_Fit_display
	NVAR BLOCHFIT_numPeaks
	WAVE BLOCHFIT_CoefTrack
	
	BLOCHFIT_Fit_display=0
	yw=0
	
	// For each peak, make a Lorentzian convolved with a Gaussian and add it to the fit output
	// Do it twice: once for the actual fit and once for the higher resolution display traces

	coefficientOffset=0
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex))
		
		Amplitude=pw[BLOCHFIT_CoefTrack[coefficientOffset]]
		Position=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]]	
		GaussFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]
		LorentzianFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+3]]
		
		//print "Peak #"+num2str(peakIndex)+", Amplitude="+num2str(Amplitude)
		// --- Fitting peak
		duplicate/O xw BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,xw)
		BLOCHFIT_TrimRange(xw)
		//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
		wavestats/Q temp_Convolution_yw
		Peak=(temp_Convolution_yw/V_max)*Amplitude
		killwaves temp_Convolution_yw
	
		// --- Display peak
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex)+"_display")
		duplicate/O BLOCHFIT_Eb_display BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,BLOCHFIT_Eb_display)
		BLOCHFIT_TrimRange(BLOCHFIT_Eb_display)
		wavestats/Q temp_Convolution_yw
		Peak=(temp_Convolution_yw/V_max)*Amplitude	
		killwaves temp_Convolution_yw
	
		coefficientOffset+=4
	endfor


	// Add the background

	variable BG_offset=pw[BLOCHFIT_CoefTrack[coefficientOffset]] // Counts
	variable BG_slope=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]]
	variable FermiLevel=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]
	variable Temperature=pw[BLOCHFIT_CoefTrack[coefficientOffset+3]]	
	variable DarkCounts=pw[BLOCHFIT_CoefTrack[coefficientOffset+4]]
	
	BLOCHFIT_Background_display=(  (BG_offset + (BG_slope*BLOCHFIT_Eb_display)) / (exp((FermiLevel-BLOCHFIT_Eb_display)/(8.617e-5*Temperature)) + 1) ) + DarkCounts

	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex))
		Peak=Peak / (exp((FermiLevel-xw)/(8.617e-5*Temperature)) + 1)
		yw=yw+Peak
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex)+"_display")
		Peak=Peak/ (exp((FermiLevel-BLOCHFIT_Eb_display)/(8.617e-5*Temperature)) + 1)
		BLOCHFIT_Fit_display=BLOCHFIT_Fit_display+Peak
		Peak=Peak+BLOCHFIT_Background_display
	endfor
	
	yw=yw+((BG_offset +(BG_slope*xw))  / (exp((FermiLevel-xw)/(8.617e-5*Temperature)) + 1))
	yw=yw+DarkCounts
	BLOCHFIT_Fit_display=BLOCHFIT_Fit_display+((BG_offset+ (BG_slope*BLOCHFIT_Eb_display))  / (exp((FermiLevel-BLOCHFIT_Eb_display)/(8.617e-5*Temperature)) + 1))
	BLOCHFIT_Fit_display=BLOCHFIT_Fit_display+DarkCounts
end




//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 	Singlet Doniach-Sunjic
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------
Function BLOCHFIT_PrepareDS(numPeaks)
//----------------------------------------------------------

	variable numPeaks
	execute "variable/G BLOCHFIT_numPeaks="+num2str(numPeaks)
	
	WAVE Intensity
	WAVE Eb
	
	if(WaveExists(Intensity) == 0 || WaveExists(Eb) == 0)
		beep
		print ">>Error<< for this fit function BLOCHFIT expects to find two specifically named input waves: [Intensity] and [Eb]"
		print "Waves with this name couldn't be found, so preparation was aborted"
		return 0
	endif
	
	variable ParametersPerPeak=5		// Amplitude, position, DS asymmetry, Gauss FWHM, DS FWHM
	variable NumExtraParameters=3	// Shirley, BG offset, BG slope
	 
	variable NumParameters=(numPeaks*ParametersPerPeak) + NumExtraParameters
	variable ii,jj
	string tempString
	Make/N=(NumParameters)/T/O BLOCHFIT_CoefficientNames	
	jj=0
	
	tempString="(Peak "+num2str(ii)+")"
	BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
	BLOCHFIT_CoefficientNames[jj+1]=tempString+" position (eV)"
	BLOCHFIT_CoefficientNames[jj+2]=tempString+" asymmetry"
	BLOCHFIT_CoefficientNames[jj+3]=tempString+" Gaussian FWHM (eV)"
	BLOCHFIT_CoefficientNames[jj+4]=tempString+" Doniach-Sunjic FWHM (eV)"
	jj+=ParametersPerPeak;
	
	for(ii=1;ii<numPeaks;ii+=1)
		tempString="(Peak "+num2str(ii)+")"
		BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
		BLOCHFIT_CoefficientNames[jj+1]=tempString+" position relative to Peak 0 (eV)"
		BLOCHFIT_CoefficientNames[jj+2]=tempString+" asymmetry"
		BLOCHFIT_CoefficientNames[jj+3]=tempString+" Gaussian FWHM (eV)"
		BLOCHFIT_CoefficientNames[jj+4]=tempString+" Doniach-Sunjic FWHM (eV)"
		jj+=ParametersPerPeak;
	endfor	
	BLOCHFIT_CoefficientNames[jj]="Shirley factor (counts per eV)"
	BLOCHFIT_CoefficientNames[jj+1]="Background offset (counts)"
	BLOCHFIT_CoefficientNames[jj+2]="Background slope (counts per eV)"
	
	
	// We can make some reasonable starting guesses for things like width and background. Otherwise leave as NaN so user has to fill in
	
	Make/N=(NumParameters)/D/O BLOCHFIT_Coefficients
	jj=0
	
	for(ii=0;ii<numPeaks;ii+=1)
		BLOCHFIT_Coefficients[jj]=NaN
		BLOCHFIT_Coefficients[jj+1]=NaN
		BLOCHFIT_Coefficients[jj+2]=0
		BLOCHFIT_Coefficients[jj+3]=0.1
		BLOCHFIT_Coefficients[jj+4]=0.1
		jj+=ParametersPerPeak;
	endfor
	wavestats/Q Intensity
	BLOCHFIT_Coefficients[jj]=0
	BLOCHFIT_Coefficients[jj+1]=V_min
	BLOCHFIT_Coefficients[jj+2]=0
	
	
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefLock
	BLOCHFIT_CoefLock=1

	Make/N=(NumParameters)/D/O BLOCHFIT_CoefTrack
	BLOCHFIT_CoefTrack=x
	
	// Make the waves we will use in the fitting graph.Some we can make higher resolution
	wavestats /Q Intensity
	Duplicate/O Intensity BLOCHFIT_Eb_display
	Redimension/N=(V_npnts*10) BLOCHFIT_Eb_display
	SetScale /I x, Eb[0], Eb[V_npnts-1], BLOCHFIT_Eb_display	//Make it have the same range
	BLOCHFIT_Eb_display=x
	
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Fit_display
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Background_display

	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Peak"+num2str(ii)+"_display"
		Execute "Duplicate/O BLOCHFIT_Eb_display "+tempString
		tempString="BLOCHFIT_Peak"+num2str(ii)
		Execute "Duplicate/O Intensity "+tempString		
	endfor
	
	// Others we need in the same resolution
	Duplicate/O Intensity BLOCHFIT_residual, BLOCHFIT_intensity




	Execute "variable/G BLOCHFIT_captionDrawn;BLOCHFIT_captionDrawn=0"
	Execute "variable/G BLOCHFIT_fitType;BLOCHFIT_fitType=5"

	BLOCHFIT_BuildDSWindow(numPeaks)

end



//----------------------------------------------------------
Function BLOCHFIT_BuildDSWindow(numPeaks)
//----------------------------------------------------------
	variable numPeaks
	variable ii
	string tempString

	// Build the fitting graph
	Execute "Display BLOCHFIT_Fit_display,BLOCHFIT_Background_display vs BLOCHFIT_Eb_display"
	
	Execute "AppendToGraph Intensity vs Eb"
	Execute "AppendToGraph/L=L2 BLOCHFIT_residual vs Eb"
	Execute "ModifyGraph zero(L2)=3,axisEnab(left)={0.15,1},axisEnab(L2)={0,0.1},freePos(L2)=0"
	Execute "ModifyGraph mode(Intensity)=3, marker(Intensity)=19, msize(Intensity)=1.5, rgb(Intensity)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_residual)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_Fit_display)=(2,39321,1)"
	Execute "ModifyGraph mode(BLOCHFIT_Background_display)=7,hbFill(BLOCHFIT_Background_display)=5, rgb(BLOCHFIT_Background_display)=(26112,26112,26112)"
	
	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Peak"+num2str(ii)+"_display"
		Execute "AppendToGraph "+tempstring+" vs BLOCHFIT_Eb_display "
	endfor
	
	Execute "Label bottom \"Binding energy (eV)\";Label left \"Intensity\""
	Execute "Legend/C/N=text0/J \"\""
	Execute "AppendText/N=text0 \"\\s(Intensity) Data\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Background_display) Background\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Fit_display) Fit\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Residual) Residual\""
	for(ii=0;ii<numPeaks;ii+=1)
		tempstring="AppendText/N=text0 \"\\s(BLOCHFIT_Peak"+num2str(ii)+"_display) Peak "+num2str(ii)+"\""
		Execute tempstring
	endfor
	Execute "Edit/K=0  BLOCHFIT_CoefficientNames,BLOCHFIT_Coefficients,BLOCHFIT_CoefLock,BLOCHFIT_CoefTrack"
	Execute "SetAxis/A/R bottom"
	
end	
	
//-------------------------------------------------------------------
Function BLOCHFIT_DS(pw,yw,xw) : FitFunc
//-------------------------------------------------------------------
	Wave pw,yw,xw
	
	variable peakIndex,coefficientOffset
	
	variable Amplitude,Position,GaussFWHM,DSFWHM,Asymmetry
	
	// Waves and variables that should already exist
	WAVE Intensity,Eb
	WAVE BLOCHFIT_Background_display
	WAVE BLOCHFIT_Eb_display
	WAVE BLOCHFIT_Fit_display
	WAVE BLOCHFIT_CoefTrack
	NVAR BLOCHFIT_numPeaks
	
	BLOCHFIT_Fit_display=0
	yw=0
	
	// For each peak, make a Lorentzian convolved with a Gaussian (twice for the two peaks in the doublet), and add them to the fit output
	// Do it twice: once for the actual fit and once for the higher resolution display traces

	coefficientOffset=0
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex))
			
		Amplitude=pw[BLOCHFIT_CoefTrack[coefficientOffset]] 
		
		Position=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]]
		if(coefficientOffset>0)
			Position=Position+pw[BLOCHFIT_CoefTrack[1]]
		endif	
		Asymmetry=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]		
		GaussFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+3]]
		DSFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+4]]
		
		// ---------------------------------------------------------------------- Make the fitting peaks
		duplicate/O xw BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=cos((Pi*Asymmetry/2) + (1-Asymmetry)*atan((Position-x)/DSFWHM) ) / ((DSFWHM^2 + (Position-x)^2)^((1-Asymmetry)/2))
		BLOCHFIT_Convolve(GaussFWHM,xw)
		BLOCHFIT_TrimRange(xw)
		//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
		wavestats/Q temp_Convolution_yw
		Peak=(temp_Convolution_yw/V_max)*Amplitude
		yw+=Peak
		killwaves temp_Convolution_yw
		
		
		// ---------------------------------------------------------------------- Make the display peaks
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex)+"_display")
		duplicate/O BLOCHFIT_Eb_display BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=cos((Pi*Asymmetry/2) + (1-Asymmetry)*atan((Position-x)/DSFWHM) ) / ((DSFWHM^2 + (Position-x)^2)^((1-Asymmetry)/2))

		BLOCHFIT_Convolve(GaussFWHM,BLOCHFIT_Eb_display)
		BLOCHFIT_TrimRange(BLOCHFIT_Eb_display)
		wavestats/Q temp_Convolution_yw
		Peak=(temp_Convolution_yw/V_max)*Amplitude	
		killwaves temp_Convolution_yw

		BLOCHFIT_Fit_display+=Peak
		
		coefficientOffset+=5
	endfor

	// ------ Generate Shirley background ------ 
	variable ShirleyCoefficient=pw[BLOCHFIT_CoefTrack[coefficientOffset]] // Counts per eV
	variable IntegratedIntensity=0
	
	// 
	
	// --- For the fit:
	duplicate/O Intensity ShirleyBackground
	ShirleyBackground=0
	wavestats/Q yw
	variable ii
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+ (xw[ii-1]-xw[ii])*((yw[ii]+yw[ii-1])/2)
		ShirleyBackground[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	ShirleyBackground[0]=ShirleyBackground[1]
	yw+=ShirleyBackground
	
	// --- For display:
	wavestats/Q BLOCHFIT_Fit_display
	IntegratedIntensity=0
	
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+  (BLOCHFIT_Eb_display[ii-1]-BLOCHFIT_Eb_display[ii])*((BLOCHFIT_Fit_display[ii]+BLOCHFIT_Fit_display[ii-1])/2)
		BLOCHFIT_Background_display[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	BLOCHFIT_Background_display[0]=BLOCHFIT_Background_display[1]
	
		
	// -----------------------------------------------------------------
	
		
	// Add the background

	variable BG_offset=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]] // Counts
	variable BG_slope=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]	
	yw = yw + BG_offset + BG_slope*(xw-xw[numpnts(xw) - 1])
	BLOCHFIT_Background_display=BLOCHFIT_Background_display+ BG_offset + BG_slope*(BLOCHFIT_Eb_display-BLOCHFIT_Eb_display[numpnts(BLOCHFIT_Eb_display) - 1])
	BLOCHFIT_Fit_display=BLOCHFIT_Fit_display+BLOCHFIT_Background_display
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Peak"+num2str(peakIndex)+"_display")
		Peak=Peak+BLOCHFIT_Background_display
	endfor
end





//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 	Doublet Voigt
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------
Function BLOCHFIT_PrepareDoubletVoigt(numPeaks)
//----------------------------------------------------------

	variable numPeaks
	execute "variable/G BLOCHFIT_numPeaks="+num2str(numPeaks)
	
	WAVE Intensity
	WAVE Eb
	
	if(WaveExists(Intensity) == 0 || WaveExists(Eb) == 0)
		beep
		print ">>Error<< for this fit function BLOCHFIT expects to find two specifically named input waves: [Intensity] and [Eb]"
		print "Waves with this name couldn't be found, so preparation was aborted"
		return 0
	endif
		
	variable ParametersPerPeak=6		// Amplitude, branching ratio, position, Gauss FWHM, Lorentz FWHM, splitting
	variable NumExtraParameters=3	// Shirley, BG offset, BG slope
	 
	variable NumParameters=(numPeaks*ParametersPerPeak) + NumExtraParameters	

	variable ii,jj
	string tempString
	Make/N=(NumParameters)/T/O BLOCHFIT_CoefficientNames	
	jj=0
	
	tempString="(Doublet "+num2str(ii)+")"
	BLOCHFIT_CoefficientNames[jj]=tempString+"  amplitude (counts)"
	BLOCHFIT_CoefficientNames[jj+1]=tempString+" branching ratio"
	BLOCHFIT_CoefficientNames[jj+2]=tempString+" position (eV)"
	BLOCHFIT_CoefficientNames[jj+3]=tempString+" Gaussian FWHM (eV)"
	BLOCHFIT_CoefficientNames[jj+4]=tempString+" Lorentzian FWHM (eV)"
	BLOCHFIT_CoefficientNames[jj+5]=tempString+" spin-orbit splitting (eV)"
	jj+=ParametersPerPeak;
	
	for(ii=1;ii<numPeaks;ii+=1)
		tempString="(Doublet "+num2str(ii)+")"
		BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
		BLOCHFIT_CoefficientNames[jj+1]=tempString+" branching ratio (counts)"
		BLOCHFIT_CoefficientNames[jj+2]=tempString+" position relative to doublet 0 (eV)"
		BLOCHFIT_CoefficientNames[jj+3]=tempString+" Gaussian FWHM (eV)"
		BLOCHFIT_CoefficientNames[jj+4]=tempString+" Lorentzian FWHM (eV)"
		BLOCHFIT_CoefficientNames[jj+5]=tempString+" spin-orbit splitting (eV)"
		jj+=ParametersPerPeak;
	endfor	
	BLOCHFIT_CoefficientNames[jj]="Shirley factor (counts per eV)"
	BLOCHFIT_CoefficientNames[jj+1]="Background offset (counts)"
	BLOCHFIT_CoefficientNames[jj+2]="Background slope (counts per eV)"
	
	
	// We can make some reasonable starting guesses for things like width and background. Otherwise leave as NaN so user has to fill in
	Make/N=(NumParameters)/D/O BLOCHFIT_Coefficients
	jj=0
	for(ii=0;ii<numPeaks;ii+=1)
		BLOCHFIT_Coefficients[jj]=NaN
		BLOCHFIT_Coefficients[jj+1]=0.5
		BLOCHFIT_Coefficients[jj+2]=NaN
		BLOCHFIT_Coefficients[jj+3]=0.3
		BLOCHFIT_Coefficients[jj+4]=0.3
		BLOCHFIT_Coefficients[jj+5]=1
		jj+=ParametersPerPeak;
	endfor
	BLOCHFIT_Coefficients[jj]=0.001
	BLOCHFIT_Coefficients[jj+1]=0
	BLOCHFIT_Coefficients[jj+2]=0
	
	
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefLock
	BLOCHFIT_CoefLock=1
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefTrack
	BLOCHFIT_CoefTrack=x	
	
	// Make the waves we will use in the fitting graph.Some we can make higher resolution
	wavestats /Q Intensity
	Duplicate/O Intensity BLOCHFIT_Eb_display
	Redimension/N=(V_npnts*10) BLOCHFIT_Eb_display
	SetScale /I x, Eb[0], Eb[V_npnts-1], BLOCHFIT_Eb_display	//Make it have the same range
	BLOCHFIT_Eb_display=x
	
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Fit_display
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Background_display

	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Doublet"+num2str(ii)+"_display"
		Execute "Duplicate/O BLOCHFIT_Eb_display "+tempString
		tempString="BLOCHFIT_Doublet"+num2str(ii)
		Execute "Duplicate/O Intensity "+tempString		
	endfor
	
	// Others we need in the same resolution
	Duplicate/O Intensity BLOCHFIT_residual, BLOCHFIT_intensity

	Execute "variable/G BLOCHFIT_captionDrawn;BLOCHFIT_captionDrawn=0"
	Execute "variable/G BLOCHFIT_fitType;BLOCHFIT_fitType=2"

	BLOCHFIT_Build2VoigtWindow(numPeaks)
	
end

//----------------------------------------------------------
Function BLOCHFIT_Build2VoigtWindow(numPeaks)
//----------------------------------------------------------
	variable numPeaks
	variable ii
	string tempString

	// Build the fitting graph
	Execute "Display BLOCHFIT_Fit_display,BLOCHFIT_Background_display vs BLOCHFIT_Eb_display"
	
	Execute "AppendToGraph Intensity vs Eb"
	Execute "AppendToGraph/L=L2 BLOCHFIT_residual vs Eb"
	Execute "ModifyGraph zero(L2)=3,axisEnab(left)={0.15,1},axisEnab(L2)={0,0.1},freePos(L2)=0"
	Execute "ModifyGraph mode(Intensity)=3, marker(Intensity)=19, msize(Intensity)=1.5, rgb(Intensity)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_residual)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_Fit_display)=(2,39321,1)"
	Execute "ModifyGraph mode(BLOCHFIT_Background_display)=7,hbFill(BLOCHFIT_Background_display)=5, rgb(BLOCHFIT_Background_display)=(26112,26112,26112)"
	
	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Doublet"+num2str(ii)+"_display"
		Execute "AppendToGraph "+tempstring+" vs BLOCHFIT_Eb_display "
	endfor
	
	Execute "Label bottom \"Binding energy (eV)\";Label left \"Intensity\""
	Execute "Legend/C/N=text0/J \"\""
	Execute "AppendText/N=text0 \"\\s(Intensity) Data\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Background_display) Background\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Fit_display) Fit\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Residual) Residual\""
	for(ii=0;ii<numPeaks;ii+=1)
		tempstring="AppendText/N=text0 \"\\s(BLOCHFIT_Doublet"+num2str(ii)+"_display) Doublet "+num2str(ii)+"\""
		Execute tempstring
	endfor
	Execute "Edit/K=0  BLOCHFIT_CoefficientNames,BLOCHFIT_Coefficients,BLOCHFIT_CoefLock,BLOCHFIT_CoefTrack"
	Execute "SetAxis/A/R bottom"
end



//-------------------------------------------------------------------
Function BLOCHFIT_DoubletVoigt(pw,yw,xw) : FitFunc
//-------------------------------------------------------------------
	Wave pw,yw,xw
	
	variable peakIndex,coefficientOffset
	
	variable Amplitude,BranchingRatio,Position,GaussFWHM,LorentzianFWHM,Splitting
	
	// Waves and variables that should already exist
	WAVE Intensity,Eb
	WAVE BLOCHFIT_Background_display
	WAVE BLOCHFIT_Eb_display
	WAVE BLOCHFIT_Fit_display
	NVAR BLOCHFIT_numPeaks
	WAVE BLOCHFIT_CoefTrack
	
	BLOCHFIT_Fit_display=0
	yw=0
	
	// For each peak, make a Lorentzian convolved with a Gaussian (twice for the two peaks in the doublet), and add them to the fit output
	// Do it twice: once for the actual fit and once for the higher resolution display traces

	coefficientOffset=0
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Doublet=$("BLOCHFIT_Doublet"+num2str(peakIndex))
		
		Amplitude=pw[BLOCHFIT_CoefTrack[coefficientOffset]]
		BranchingRatio=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]] // no units
		Position=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]
		if(coefficientOffset>0)
			Position=Position+pw[BLOCHFIT_CoefTrack[2]] //Position is relative to first peak
		endif	
		GaussFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+3]]
		LorentzianFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+4]]
		Splitting=pw[BLOCHFIT_CoefTrack[coefficientOffset+5]]
		
		// ---------------------------------------------------------------------- Make the fitting peaks
		duplicate/O xw BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,xw)
		BLOCHFIT_TrimRange(xw)
		//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
		wavestats/Q temp_Convolution_yw
		Doublet=(temp_Convolution_yw/V_max)*Amplitude
		killwaves temp_Convolution_yw
	
		duplicate/O xw BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position-Splitting)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,xw)
		BLOCHFIT_TrimRange(xw)
		//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
		wavestats/Q temp_Convolution_yw
		Doublet=Doublet+((temp_Convolution_yw/V_max)*Amplitude*BranchingRatio)
		yw+=Doublet
		killwaves temp_Convolution_yw
		
		
		// ---------------------------------------------------------------------- Make the display peaks
		WAVE Doublet=$("BLOCHFIT_Doublet"+num2str(peakIndex)+"_display")
		duplicate/O BLOCHFIT_Eb_display BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,BLOCHFIT_Eb_display)
		BLOCHFIT_TrimRange(BLOCHFIT_Eb_display)
		wavestats/Q temp_Convolution_yw
		Doublet=(temp_Convolution_yw/V_max)*Amplitude	
		killwaves temp_Convolution_yw

		duplicate/O BLOCHFIT_Eb_display BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=(1/ (1+4*((x-Position-Splitting)/LorentzianFWHM )^2))  	//Lorentzian
		BLOCHFIT_Convolve(GaussFWHM,BLOCHFIT_Eb_display)
		BLOCHFIT_TrimRange(BLOCHFIT_Eb_display)
		wavestats/Q temp_Convolution_yw
		Doublet=Doublet+((temp_Convolution_yw/V_max)*Amplitude*BranchingRatio)	
		killwaves temp_Convolution_yw		
		BLOCHFIT_Fit_display+=Doublet
		
		coefficientOffset+=6
	endfor

	// ------ Generate Shirley background ------ 
	variable ShirleyCoefficient=pw[BLOCHFIT_CoefTrack[coefficientOffset]] // Counts per eV
	variable IntegratedIntensity=0
	
	// 
	
	// --- For the fit:
	duplicate/O Intensity ShirleyBackground
	ShirleyBackground=0
	wavestats/Q yw
	variable ii
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+ (xw[ii-1]-xw[ii])*((yw[ii]+yw[ii-1])/2)
		ShirleyBackground[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	ShirleyBackground[0]=ShirleyBackground[1]
	yw+=ShirleyBackground
	
	// --- For display:
	wavestats/Q BLOCHFIT_Fit_display
	IntegratedIntensity=0
	
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+  (BLOCHFIT_Eb_display[ii-1]-BLOCHFIT_Eb_display[ii])*((BLOCHFIT_Fit_display[ii]+BLOCHFIT_Fit_display[ii-1])/2)
		BLOCHFIT_Background_display[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	BLOCHFIT_Background_display[0]=BLOCHFIT_Background_display[1]
	
		
	// -----------------------------------------------------------------
	
		
	// Add the background

	variable BG_offset=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]] // Counts
	variable BG_slope=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]	
	yw = yw + BG_offset + BG_slope*(xw-xw[numpnts(xw) - 1])
	BLOCHFIT_Background_display=BLOCHFIT_Background_display+ BG_offset + BG_slope*(BLOCHFIT_Eb_display-BLOCHFIT_Eb_display[numpnts(BLOCHFIT_Eb_display) - 1])
	BLOCHFIT_Fit_display=BLOCHFIT_Fit_display+BLOCHFIT_Background_display
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Doublet"+num2str(peakIndex)+"_display")
		Peak=Peak+BLOCHFIT_Background_display
	endfor
end



//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 	Doublet Doniach-Sunjic
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------
Function BLOCHFIT_PrepareDoubletDS(numPeaks)
//----------------------------------------------------------

	variable numPeaks
	execute "variable/G BLOCHFIT_numPeaks="+num2str(numPeaks)
	
	WAVE Intensity
	WAVE Eb
	
	if(WaveExists(Intensity) == 0 || WaveExists(Eb) == 0)
		beep
		print ">>Error<< for this fit function BLOCHFIT expects to find two specifically named input waves: [Intensity] and [Eb]"
		print "Waves with this name couldn't be found, so preparation was aborted"
		return 0
	endif
		
	variable ParametersPerPeak=7		// Amplitude, branching ratio, position, DS asymmetry, Gauss FWHM, DS FWHM, splitting
	variable NumExtraParameters=3	// Shirley, BG offset, BG slope
	 
	variable NumParameters=(numPeaks*ParametersPerPeak) + NumExtraParameters
	variable ii,jj
	string tempString
	Make/N=(NumParameters)/T/O BLOCHFIT_CoefficientNames	
	jj=0
	
	tempString="(Doublet "+num2str(ii)+")"
	BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
	BLOCHFIT_CoefficientNames[jj+1]=tempString+" branching ratio"
	BLOCHFIT_CoefficientNames[jj+2]=tempString+" position (eV)"
	BLOCHFIT_CoefficientNames[jj+3]=tempString+" asymmetry"
	BLOCHFIT_CoefficientNames[jj+4]=tempString+" Gaussian FWHM (eV)"
	BLOCHFIT_CoefficientNames[jj+5]=tempString+" Doniach-Sunjic FWHM (eV)"
	BLOCHFIT_CoefficientNames[jj+6]=tempString+" spin-orbit splitting (eV)"
	jj+=ParametersPerPeak;
	
	for(ii=1;ii<numPeaks;ii+=1)
		tempString="(Doublet "+num2str(ii)+")"
		BLOCHFIT_CoefficientNames[jj]=tempString+" amplitude (counts)"
		BLOCHFIT_CoefficientNames[jj+1]=tempString+" branching ratio"
		BLOCHFIT_CoefficientNames[jj+2]=tempString+" position relative to doublet 0 (eV)"
		BLOCHFIT_CoefficientNames[jj+3]=tempString+" asymmetry"
		BLOCHFIT_CoefficientNames[jj+4]=tempString+" Gaussian FWHM (eV)"
		BLOCHFIT_CoefficientNames[jj+5]=tempString+" Doniach-Sunjic FWHM (eV)"
		BLOCHFIT_CoefficientNames[jj+6]=tempString+" spin-orbit splitting (eV)"
		jj+=ParametersPerPeak;
	endfor	
	BLOCHFIT_CoefficientNames[jj]="Shirley factor (counts per eV)"
	BLOCHFIT_CoefficientNames[jj+1]="Background offset (counts)"
	BLOCHFIT_CoefficientNames[jj+2]="Background slope (counts per eV)"
	
	
	// We can make some reasonable starting guesses for things like width and background. Otherwise leave as NaN so user has to fill in
	Make/N=(NumParameters)/D/O BLOCHFIT_Coefficients
	jj=0
	for(ii=0;ii<numPeaks;ii+=1)
		BLOCHFIT_Coefficients[jj]=NaN
		BLOCHFIT_Coefficients[jj+1]=0.5
		BLOCHFIT_Coefficients[jj+2]=NaN
		BLOCHFIT_Coefficients[jj+3]=0
		BLOCHFIT_Coefficients[jj+4]=0.3
		BLOCHFIT_Coefficients[jj+5]=0.3
		BLOCHFIT_Coefficients[jj+6]=1
		jj+=ParametersPerPeak;
	endfor
	BLOCHFIT_Coefficients[jj]=0.001
	BLOCHFIT_Coefficients[jj+1]=0
	BLOCHFIT_Coefficients[jj+2]=0
	
	
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefLock
	BLOCHFIT_CoefLock=1

	Make/N=(NumParameters)/D/O BLOCHFIT_CoefTrack
	BLOCHFIT_CoefTrack=x
	
	// Make the waves we will use in the fitting graph.Some we can make higher resolution
	wavestats /Q Intensity
	Duplicate/O Intensity BLOCHFIT_Eb_display
	Redimension/N=(V_npnts*10) BLOCHFIT_Eb_display
	SetScale /I x, Eb[0], Eb[V_npnts-1], BLOCHFIT_Eb_display	//Make it have the same range
	BLOCHFIT_Eb_display=x
	
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Fit_display
	Duplicate/O BLOCHFIT_Eb_display BLOCHFIT_Background_display

	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Doublet"+num2str(ii)+"_display"
		Execute "Duplicate/O BLOCHFIT_Eb_display "+tempString
		tempString="BLOCHFIT_Doublet"+num2str(ii)
		Execute "Duplicate/O Intensity "+tempString		
	endfor
	
	// Others we need in the same resolution
	Duplicate/O Intensity BLOCHFIT_residual, BLOCHFIT_intensity



	Execute "variable/G BLOCHFIT_captionDrawn;BLOCHFIT_captionDrawn=0"
	Execute "variable/G BLOCHFIT_fitType;BLOCHFIT_fitType=3"

	BLOCHFIT_Build2DSWindow(numPeaks)

end


//----------------------------------------------------------
Function BLOCHFIT_Build2DSWindow(numPeaks)
//----------------------------------------------------------
	variable numPeaks
	variable ii
	string tempString

	// Build the fitting graph
	Execute "Display BLOCHFIT_Fit_display,BLOCHFIT_Background_display vs BLOCHFIT_Eb_display"
	
	Execute "AppendToGraph Intensity vs Eb"
	Execute "AppendToGraph/L=L2 BLOCHFIT_residual vs Eb"
	Execute "ModifyGraph zero(L2)=3,axisEnab(left)={0.15,1},axisEnab(L2)={0,0.1},freePos(L2)=0"
	Execute "ModifyGraph mode(Intensity)=3, marker(Intensity)=19, msize(Intensity)=1.5, rgb(Intensity)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_residual)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_Fit_display)=(2,39321,1)"
	Execute "ModifyGraph mode(BLOCHFIT_Background_display)=7,hbFill(BLOCHFIT_Background_display)=5, rgb(BLOCHFIT_Background_display)=(26112,26112,26112)"
	
	for(ii=0;ii<numPeaks;ii+=1)
		tempString="BLOCHFIT_Doublet"+num2str(ii)+"_display"
		Execute "AppendToGraph "+tempstring+" vs BLOCHFIT_Eb_display "
	endfor
	
	Execute "Label bottom \"Binding energy (eV)\";Label left \"Intensity\""
	Execute "Legend/C/N=text0/J \"\""
	Execute "AppendText/N=text0 \"\\s(Intensity) Data\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Background_display) Background\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Fit_display) Fit\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Residual) Residual\""
	for(ii=0;ii<numPeaks;ii+=1)
		tempstring="AppendText/N=text0 \"\\s(BLOCHFIT_Doublet"+num2str(ii)+"_display) Doublet "+num2str(ii)+"\""
		Execute tempstring
	endfor
	Execute "Edit/K=0  BLOCHFIT_CoefficientNames,BLOCHFIT_Coefficients,BLOCHFIT_CoefLock,BLOCHFIT_CoefTrack"
	Execute "SetAxis/A/R bottom"
end

//-------------------------------------------------------------------
Function BLOCHFIT_DoubletDS(pw,yw,xw) : FitFunc
//-------------------------------------------------------------------
	Wave pw,yw,xw
	
	variable peakIndex,coefficientOffset
	
	variable Amplitude,BranchingRatio,Position,GaussFWHM,DSFWHM,Splitting,Asymmetry
	
	// Waves and variables that should already exist
	WAVE Intensity,Eb
	WAVE BLOCHFIT_Background_display
	WAVE BLOCHFIT_Eb_display
	WAVE BLOCHFIT_Fit_display
	WAVE BLOCHFIT_CoefTrack
	NVAR BLOCHFIT_numPeaks
	
	BLOCHFIT_Fit_display=0
	yw=0
	
	// For each peak, make a Lorentzian convolved with a Gaussian (twice for the two peaks in the doublet), and add them to the fit output
	// Do it twice: once for the actual fit and once for the higher resolution display traces

	coefficientOffset=0
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Doublet=$("BLOCHFIT_Doublet"+num2str(peakIndex))
			
		Amplitude=pw[BLOCHFIT_CoefTrack[coefficientOffset]] 
		BranchingRatio=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]] // no units
		Position=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]
		if(coefficientOffset>0)
			Position=Position+pw[BLOCHFIT_CoefTrack[2]]
		endif	
		Asymmetry=pw[BLOCHFIT_CoefTrack[coefficientOffset+3]]		
		GaussFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+4]]
		DSFWHM=pw[BLOCHFIT_CoefTrack[coefficientOffset+5]]
		Splitting=pw[BLOCHFIT_CoefTrack[coefficientOffset+6]]
		
		// ---------------------------------------------------------------------- Make the fitting peaks
		duplicate/O xw BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=cos((Pi*Asymmetry/2) + (1-Asymmetry)*atan((Position-x)/DSFWHM) ) / ((DSFWHM^2 + (Position-x)^2)^((1-Asymmetry)/2))
		BLOCHFIT_Convolve(GaussFWHM,xw)
		BLOCHFIT_TrimRange(xw)
		//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
		wavestats/Q temp_Convolution_yw
		Doublet=(temp_Convolution_yw/V_max)*Amplitude
		killwaves temp_Convolution_yw
	
		duplicate/O xw BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=cos((Pi*Asymmetry/2) + (1-Asymmetry)*atan((Position+Splitting-x)/DSFWHM) ) / ((DSFWHM^2 + (Position+Splitting-x)^2)^((1-Asymmetry)/2))
		BLOCHFIT_Convolve(GaussFWHM,xw)
		BLOCHFIT_TrimRange(xw)
		//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
		wavestats/Q temp_Convolution_yw
		Doublet=Doublet+((temp_Convolution_yw/V_max)*Amplitude*BranchingRatio)
		yw+=Doublet
		killwaves temp_Convolution_yw
		
		
		// ---------------------------------------------------------------------- Make the display peaks
		WAVE Doublet=$("BLOCHFIT_Doublet"+num2str(peakIndex)+"_display")
		duplicate/O BLOCHFIT_Eb_display BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=cos((Pi*Asymmetry/2) + (1-Asymmetry)*atan((Position-x)/DSFWHM) ) / ((DSFWHM^2 + (Position-x)^2)^((1-Asymmetry)/2))

		BLOCHFIT_Convolve(GaussFWHM,BLOCHFIT_Eb_display)
		BLOCHFIT_TrimRange(BLOCHFIT_Eb_display)
		wavestats/Q temp_Convolution_yw
		Doublet=(temp_Convolution_yw/V_max)*Amplitude	
		killwaves temp_Convolution_yw

		duplicate/O BLOCHFIT_Eb_display BLOCHFIT_xw
		BLOCHFIT_ExtendRange()	
		WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
		temp_Convolution_yw=cos((Pi*Asymmetry/2) + (1-Asymmetry)*atan((Position+Splitting-x)/DSFWHM) ) / ((DSFWHM^2 + (Position+Splitting-x)^2)^((1-Asymmetry)/2))
		BLOCHFIT_Convolve(GaussFWHM,BLOCHFIT_Eb_display)
		BLOCHFIT_TrimRange(BLOCHFIT_Eb_display)
		wavestats/Q temp_Convolution_yw
		Doublet=Doublet+((temp_Convolution_yw/V_max)*Amplitude*BranchingRatio)	
		killwaves temp_Convolution_yw		
		BLOCHFIT_Fit_display+=Doublet
		
		coefficientOffset+=7
	endfor

	// ------ Generate Shirley background ------ 
	variable ShirleyCoefficient=pw[BLOCHFIT_CoefTrack[coefficientOffset]] // Counts per eV
	variable IntegratedIntensity=0
	
	// 
	
	// --- For the fit:
	duplicate/O Intensity ShirleyBackground
	ShirleyBackground=0
	wavestats/Q yw
	variable ii
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+ (xw[ii-1]-xw[ii])*((yw[ii]+yw[ii-1])/2)
		ShirleyBackground[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	ShirleyBackground[0]=ShirleyBackground[1]
	yw+=ShirleyBackground
	
	// --- For display:
	wavestats/Q BLOCHFIT_Fit_display
	IntegratedIntensity=0
	
	for(ii=V_npnts-1;ii>0;ii=ii-1)
		IntegratedIntensity = IntegratedIntensity+  (BLOCHFIT_Eb_display[ii-1]-BLOCHFIT_Eb_display[ii])*((BLOCHFIT_Fit_display[ii]+BLOCHFIT_Fit_display[ii-1])/2)
		BLOCHFIT_Background_display[ii] = ShirleyCoefficient*IntegratedIntensity	
	endfor
	BLOCHFIT_Background_display[0]=BLOCHFIT_Background_display[1]
	
		
	// -----------------------------------------------------------------
	
		
	// Add the background

	variable BG_offset=pw[BLOCHFIT_CoefTrack[coefficientOffset+1]] // Counts
	variable BG_slope=pw[BLOCHFIT_CoefTrack[coefficientOffset+2]]	
	yw = yw + BG_offset + BG_slope*(xw-xw[numpnts(xw) - 1])
	BLOCHFIT_Background_display=BLOCHFIT_Background_display+ BG_offset + BG_slope*(BLOCHFIT_Eb_display-BLOCHFIT_Eb_display[numpnts(BLOCHFIT_Eb_display) - 1])
	BLOCHFIT_Fit_display=BLOCHFIT_Fit_display+BLOCHFIT_Background_display
	for(peakIndex=0;peakIndex<BLOCHFIT_numPeaks;peakIndex+=1)
		WAVE Peak=$("BLOCHFIT_Doublet"+num2str(peakIndex)+"_display")
		Peak=Peak+BLOCHFIT_Background_display
	endfor
end


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 	Fermi edge [simple]
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------
Function BLOCHFIT_PrepareSimpleFermiEdge()
//----------------------------------------------------------

	WAVE Intensity
	WAVE Ek
	
	if(WaveExists(Intensity) == 0 || WaveExists(Ek) == 0)
		beep
		print ">>Error<< for this fit function BLOCHFIT expects to find two specifically named input waves: [Intensity] and [Ek]"
		print "Waves with this name couldn't be found, so preparation was aborted"
		return 0
	endif
		
	variable NumParameters=5
	
	Make/N=(NumParameters)/T/O BLOCHFIT_CoefficientNames
	BLOCHFIT_CoefficientNames[0]="Amplitude"
	BLOCHFIT_CoefficientNames[1]="FermiLevel (eV)"
	BLOCHFIT_CoefficientNames[2]="Sample temperature (K)"
	BLOCHFIT_CoefficientNames[3]="Broadening FWHM (eV)"
	BLOCHFIT_CoefficientNames[4]="Background offset"
	
	// Miniumum of the derivative gives a good starting guess of the fermi level
	Differentiate Intensity/X=Ek/D=Intensity_DIF
	Intensity_DIF=Intensity_DIF
	wavestats/Q Intensity_DIF
	KillWaves Intensity_DIF
		
	Make/N=(NumParameters)/D/O BLOCHFIT_Coefficients
	BLOCHFIT_Coefficients[0]=Intensity[0]-Intensity[V_npnts-1]
	BLOCHFIT_Coefficients[1]=Ek[V_minRowLoc]  //The energy value corresponding to the minimum of the differentiated intensity
	BLOCHFIT_Coefficients[2]=80
	BLOCHFIT_Coefficients[3]=0.05
	BLOCHFIT_Coefficients[4]=Intensity[V_npnts-1] //Last value of the Intensity wave, good estimate of background offset
	
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefLock
	BLOCHFIT_CoefLock=1
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefTrack
	BLOCHFIT_CoefTrack=x
		
	// Make the waves we will use in the fitting graph.Some we can make higher resolution
	wavestats /Q Intensity
	Duplicate/O Intensity BLOCHFIT_Ek_display
	Redimension/N=(V_npnts*10) BLOCHFIT_Ek_display
	SetScale /I x, Ek[0], Ek[V_npnts-1], BLOCHFIT_Ek_display	//Make it have the same range
	BLOCHFIT_Ek_display=x
	Duplicate/O BLOCHFIT_Ek_display BLOCHFIT_Fit_display
	Duplicate/O BLOCHFIT_Ek_display BLOCHFIT_Background_display
	
	// Others we need in the same resolution
	Duplicate/O Intensity BLOCHFIT_residual, BLOCHFIT_intensity

	Execute "variable/G BLOCHFIT_captionDrawn;BLOCHFIT_captionDrawn=0"
	Execute "variable/G BLOCHFIT_fitType;BLOCHFIT_fitType=6"

	BLOCHFIT_BuildSimpleEFWindow()
	
	BLOCHFIT_Execute()

end


//----------------------------------------------------------
Function BLOCHFIT_BuildSimpleEFWindow()
//----------------------------------------------------------
	variable numPeaks
	variable ii
	string tempString

	// Build the fitting graph
	Execute "Display BLOCHFIT_Fit_display,BLOCHFIT_Background_display vs BLOCHFIT_Ek_display"
	
	Execute "AppendToGraph Intensity vs Ek"
	Execute "AppendToGraph/L=L2 BLOCHFIT_residual vs Ek"
	Execute "ModifyGraph zero(L2)=3,axisEnab(left)={0.15,1},axisEnab(L2)={0,0.1},freePos(L2)=0"
	Execute "ModifyGraph mode(Intensity)=3, marker(Intensity)=19, msize(Intensity)=1.5, rgb(Intensity)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_residual)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_Fit_display)=(2,39321,1)"
	Execute "ModifyGraph mode(BLOCHFIT_Background_display)=7,hbFill(BLOCHFIT_Background_display)=5, rgb(BLOCHFIT_Background_display)=(26112,26112,26112)"
	
	Execute "Label bottom \"Kinetic energy (eV)\";Label left \"Intensity\""
	Execute "Legend/C/N=text0/J \"\""
	Execute "AppendText/N=text0 \"\\s(Intensity) Data\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Background_display) Background\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Fit_display) Fit\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Residual) Residual\""
	Execute "Edit/K=0  BLOCHFIT_CoefficientNames,BLOCHFIT_Coefficients,BLOCHFIT_CoefLock"

end

//-------------------------------------------------------------------
Function BLOCHFIT_SimpleFermiEdge(pw,yw,xw) : FitFunc
//-------------------------------------------------------------------
	Wave pw,yw,xw
	
	// Waves that should already exist
	WAVE Intensity,Ek
	WAVE BLOCHFIT_UnbroadenedFermiEdge
	WAVE BLOCHFIT_Background_display
	WAVE BLOCHFIT_Ek_display
	WAVE BLOCHFIT_Fit_display

	variable Amplitude=pw[0]
	variable FermiLevel=pw[1]
	variable Temperature=pw[2]
	variable BroadeningFWHM=pw[3]
	variable BG_offset=pw[4]
		
	//--Convolution
	duplicate/O xw BLOCHFIT_xw
	BLOCHFIT_ExtendRange()	
	WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
	temp_Convolution_yw=1 / (exp((x-FermiLevel)/(8.617e-5*Temperature)) + 1)
	BLOCHFIT_Convolve(BroadeningFWHM,xw)
	BLOCHFIT_TrimRange(xw)
	//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
	wavestats/Q temp_Convolution_yw
	yw=Amplitude / (exp((xw-FermiLevel)/(8.617e-5*Temperature)) + 1)
	yw=(temp_Convolution_yw/V_max)*Amplitude
	killwaves temp_Convolution_yw
	
	
	
	// Add the background
	yw = yw + BG_offset

	//These waves are just for display:
	BLOCHFIT_Background_display = BG_offset	

	//--Convolution
	duplicate/O BLOCHFIT_Ek_display BLOCHFIT_xw
	BLOCHFIT_ExtendRange()	
	WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
	temp_Convolution_yw=Amplitude / (exp((x-FermiLevel)/(8.617e-5*Temperature)) + 1)
	BLOCHFIT_Convolve(BroadeningFWHM,BLOCHFIT_Ek_display)
	BLOCHFIT_TrimRange(BLOCHFIT_Ek_display)

	wavestats/Q temp_Convolution_yw
	BLOCHFIT_Fit_display=((temp_Convolution_yw/V_max)*Amplitude) + (BLOCHFIT_Background_display)
	killwaves temp_Convolution_yw


end


		
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 	Fermi edge [resolution testing]
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------
Function BLOCHFIT_PrepareFermiEdge()
//----------------------------------------------------------

	WAVE Intensity
	WAVE Ek
	
	if(WaveExists(Intensity) == 0 || WaveExists(Ek) == 0)
		beep
		print ">>Error<< for this fit function BLOCHFIT expects to find two specifically named input waves: [Intensity] and [Ek]"
		print "Waves with this name couldn't be found, so preparation was aborted"
		return 0
	endif
		
	variable NumParameters=7
	
	Make/N=(NumParameters)/T/O BLOCHFIT_CoefficientNames
	BLOCHFIT_CoefficientNames[0]="Amplitude"
	BLOCHFIT_CoefficientNames[1]="FermiLevel (eV)"
	BLOCHFIT_CoefficientNames[2]="Sample temperature (K)"
	BLOCHFIT_CoefficientNames[3]="Light source broadening FWHM (eV)"
	BLOCHFIT_CoefficientNames[4]="Analyzer broadening FWHM (eV)"
	BLOCHFIT_CoefficientNames[5]="Background offset"
	BLOCHFIT_CoefficientNames[6]="Background slope"
	
	// Miniumum of the derivative gives a good starting guess of the fermi level
	Differentiate Intensity/X=Ek/D=Intensity_DIF
	Intensity_DIF=Intensity_DIF
	wavestats/Q Intensity_DIF
	KillWaves Intensity_DIF
		
	Make/N=(NumParameters)/D/O BLOCHFIT_Coefficients
	BLOCHFIT_Coefficients[0]=Intensity[0]-Intensity[V_npnts-1]
	BLOCHFIT_Coefficients[1]=Ek[V_minRowLoc]  //The energy value corresponding to the minimum of the differentiated intensity
	BLOCHFIT_Coefficients[2]=15
	BLOCHFIT_Coefficients[3]=0.02
	BLOCHFIT_Coefficients[4]=0.05
	BLOCHFIT_Coefficients[5]=Intensity[V_npnts-1] //Last value of the Intensity wave, good estimate of background offset
	BLOCHFIT_Coefficients[6]=0
	
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefLock
	BLOCHFIT_CoefLock=1
	Make/N=(NumParameters)/D/O BLOCHFIT_CoefTrack
	BLOCHFIT_CoefTrack=x
		
	// Make the waves we will use in the fitting graph.Some we can make higher resolution
	wavestats /Q Intensity
	Duplicate/O Intensity BLOCHFIT_Ek_display
	Redimension/N=(V_npnts*10) BLOCHFIT_Ek_display
	SetScale /I x, Ek[0], Ek[V_npnts-1], BLOCHFIT_Ek_display	//Make it have the same range
	BLOCHFIT_Ek_display=x
	Duplicate/O BLOCHFIT_Ek_display BLOCHFIT_Fit_display
	Duplicate/O BLOCHFIT_Ek_display BLOCHFIT_Background_display
	Duplicate/O BLOCHFIT_Ek_display BLOCHFIT_UnbroadenedFermiEdge
	
	// Others we need in the same resolution
	Duplicate/O Intensity BLOCHFIT_residual, BLOCHFIT_intensity



	Execute "variable/G BLOCHFIT_captionDrawn;BLOCHFIT_captionDrawn=0"
	Execute "variable/G BLOCHFIT_fitType;BLOCHFIT_fitType=0"
	
	BLOCHFIT_BuildFermiWindow()

	BLOCHFIT_Execute()

end


//----------------------------------------------------------
Function BLOCHFIT_BuildFermiWindow()
//----------------------------------------------------------
	variable numPeaks
	variable ii
	string tempString

	Execute "Display BLOCHFIT_Fit_display,BLOCHFIT_Background_display,BLOCHFIT_UnbroadenedFermiEdge vs BLOCHFIT_Ek_display"
	
	Execute "AppendToGraph Intensity vs Ek"
	Execute "AppendToGraph/L=L2 BLOCHFIT_residual vs Ek"
	Execute "ModifyGraph zero(L2)=3,axisEnab(left)={0.15,1},axisEnab(L2)={0,0.1},freePos(L2)=0"
	Execute "ModifyGraph mode(Intensity)=3, marker(Intensity)=19, msize(Intensity)=1.5, rgb(Intensity)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_residual)=(0,0,0)"
	Execute "ModifyGraph rgb(BLOCHFIT_Fit_display)=(2,39321,1)"
	Execute "ModifyGraph mode(BLOCHFIT_Background_display)=7,hbFill(BLOCHFIT_Background_display)=5, rgb(BLOCHFIT_Background_display)=(26112,26112,26112)"
	
	Execute "Label bottom \"Kinetic energy (eV)\";Label left \"Intensity\""
	Execute "Legend/C/N=text0/J \"\""
	Execute "AppendText/N=text0 \"\\s(Intensity) Data\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Background_display) Background\""	
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_UnbroadenedFermiEdge) Only temperature broadening\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Fit_display) Fit\""
	Execute "AppendText/N=text0 \"\\s(BLOCHFIT_Residual) Residual\""
	Execute "Edit/K=0  BLOCHFIT_CoefficientNames,BLOCHFIT_Coefficients,BLOCHFIT_CoefLock"
end



//-------------------------------------------------------------------
Function BLOCHFIT_FermiEdge(pw,yw,xw) : FitFunc
//-------------------------------------------------------------------
	Wave pw,yw,xw
	
	// Waves that should already exist
	WAVE Intensity,Ek
	WAVE BLOCHFIT_UnbroadenedFermiEdge
	WAVE BLOCHFIT_Background_display
	WAVE BLOCHFIT_Ek_display
	WAVE BLOCHFIT_Fit_display

	variable Amplitude=pw[0]
	variable FermiLevel=pw[1]
	variable Temperature=pw[2]
	variable SourceFWHM=pw[3]
	variable AnalyzerFWHM=pw[4]
	variable BG_offset=pw[5]
	variable BG_slope=pw[6]
		
	//--Convolution
	duplicate/O xw BLOCHFIT_xw
	BLOCHFIT_ExtendRange()	
	WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
	temp_Convolution_yw=1 / (exp((x-FermiLevel)/(8.617e-5*Temperature)) + 1)
	BLOCHFIT_Convolve(sqrt(SourceFWHM^2 + AnalyzerFWHM^2),xw)
	BLOCHFIT_TrimRange(xw)
	//The convolution causes some rescaling, so renormalize then scale to the right amplitude 
	wavestats/Q temp_Convolution_yw
	yw=(temp_Convolution_yw/V_max)*Amplitude
	killwaves temp_Convolution_yw
	
	
	
	// Add the background
	yw = yw + BG_offset + BG_slope*(xw-xw[numpnts(xw) - 1])
	//These waves are just for display:
	BLOCHFIT_Background_display = BG_offset + BG_slope*(BLOCHFIT_Ek_display-BLOCHFIT_Ek_display[numpnts(BLOCHFIT_Ek_display) - 1])	
	BLOCHFIT_UnbroadenedFermiEdge=(Amplitude / (exp((BLOCHFIT_Ek_display-FermiLevel)/(8.617e-5*Temperature)) + 1)	) + (BLOCHFIT_Background_display)

	//--Convolution
	duplicate/O BLOCHFIT_Ek_display BLOCHFIT_xw
	BLOCHFIT_ExtendRange()	
	WAVE temp_Convolution_yw  //gain awareness of the wave created by BLOCHFIT_ExtendRange()
	temp_Convolution_yw=Amplitude / (exp((x-FermiLevel)/(8.617e-5*Temperature)) + 1)
	BLOCHFIT_Convolve(sqrt(SourceFWHM^2 + AnalyzerFWHM^2),BLOCHFIT_Ek_display)
	BLOCHFIT_TrimRange(BLOCHFIT_Ek_display)

	wavestats/Q temp_Convolution_yw
	BLOCHFIT_Fit_display=((temp_Convolution_yw/V_max)*Amplitude) + (BLOCHFIT_Background_display)
	killwaves temp_Convolution_yw


end





